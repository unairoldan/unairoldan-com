import React from "react"
import renderer from "react-test-renderer"

import BlogPost from "../../src/templates/blog-post"
import siteMetadata from "../../src/components/SiteMetadata"
import siteData from "../__mocks__/useSiteMetadata.json"
import article from "../__mocks__/article.json"
import articleWithoutTags from "./__mocks__/article-without-tags.json"

jest.mock('../../src/components/SiteMetadata', () => jest.fn())
siteMetadata.mockReturnValue(siteData)

describe("BlogPage", () => {

  const pageContext = {
    slug: "/scrum-master-certification-comparison/"
  }

  it("renders correctly article", () => {
    const tree = renderer
      .create(<BlogPost data={article} pageContext={pageContext} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("renders correctly article without tags", () => {
    const tree = renderer
      .create(<BlogPost data={articleWithoutTags} pageContext={pageContext} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})