import React from "react"
import renderer from "react-test-renderer"

import IndexPage from "../../src/templates/index-template"
import siteMetadata from "../../src/components/SiteMetadata"
import BlogRoll from "../../src/components/BlogRoll"
import siteData from "../__mocks__/useSiteMetadata.json"
import dataEs from "./__mocks__/index-es.json"
import dataEn from "./__mocks__/index-en.json"

jest.mock('../../src/components/SiteMetadata', () => jest.fn())
siteMetadata.mockReturnValue(siteData)

jest.mock('../../src/components/BlogRoll', () => jest.fn())
BlogRoll.mockReturnValue('BlogRoll')

describe("IndexPage", () => {

  const pageContext = {
    slug: "/"
  }

  it("renders correctly in Spanish", () => {
    const tree = renderer
      .create(<IndexPage data={dataEs} pageContext={pageContext} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("renders correctly in English, without fluid image", () => {
    const tree = renderer
      .create(<IndexPage data={dataEn} pageContext={pageContext} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})