import React from "react"
import renderer from "react-test-renderer"

import TagsPage from "../../src/templates/tags"
import siteMetadata from "../../src/components/SiteMetadata"
import siteData from "../__mocks__/useSiteMetadata.json"
import data from "./__mocks__/tag.json"

jest.mock('../../src/components/SiteMetadata', () => jest.fn())
siteMetadata.mockReturnValue(siteData)

describe("TagRoute", () => {

  const pageContext = {
    slug: "scrum"
  }

  const location = {
    pathname: "/scrum/"
  }

  it("renders correctly in Spanish", () => {
    const tree = renderer
      .create(<TagsPage data={data} pageContext={pageContext} location={location} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})