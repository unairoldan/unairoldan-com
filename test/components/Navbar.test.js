import React from "react"
import renderer from "react-test-renderer"
import { mount } from 'enzyme';

import Navbar from "../../src/components/Navbar"

describe("Navbar", () => {

  it("renders correctly in Spanish", () => {
    const tree = renderer
      .create(<Navbar language="es" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  }),
  
  it("renders correctly in English", () => {
    const tree = renderer
      .create(<Navbar language="en" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
  
  it('check menu popup open/close', () => {  
    const NavbarComponent = mount(<Navbar language="es" />),
    menu = NavbarComponent.find("div.navbar-burger");
    expect(NavbarComponent.find('div.is-active')).toHaveLength(0);
    menu.simulate('keydown');
    expect(NavbarComponent.find('div.is-active')).toHaveLength(2);
    menu.simulate('click');
    expect(NavbarComponent.find('div.is-active')).toHaveLength(0);
  })

})