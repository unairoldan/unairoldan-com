import React from "react"
import renderer from "react-test-renderer"

import ShareThis from "../../src/components/ShareThis"

describe("ShareThis", () => {

  it("renders correctly in Spanish", () => {
    const tree = renderer
      .create(<ShareThis
        text="Text to share"
        profile="unairoldan"
        url="https://www.unairoldan.com"
        tags="Agile,Organization,BusinessAgility"
        language="es" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  }),

  it("renders correctly in English", () => {
    const tree = renderer
      .create(<ShareThis
        text="Text to share"
        profile="unairoldan"
        url="https://www.unairoldan.com"
        tags="Agile,Organization,BusinessAgility"
        language="es" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})