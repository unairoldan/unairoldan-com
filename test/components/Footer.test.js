import React from "react"
import renderer from "react-test-renderer"

import Footer from "../../src/components/Footer"

describe("Footer", () => {

  it("renders correctly in Spanish", () => {
    const tree = renderer
      .create(<Footer language="es" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  }),
  
  it("renders correctly in English", () => {
    const tree = renderer
      .create(<Footer language="en" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})