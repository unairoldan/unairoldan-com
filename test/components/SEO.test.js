import React from "react"
import renderer from "react-test-renderer"
import { shallow } from 'enzyme';

import SEO from "../../src/components/SEO"
import seoBlog from "./__mocks__/SEO-blog.json"
import seoArticle from "./__mocks__/SEO-article.json"

describe("SEO ", () => {

  it("renders correctly", () => {
    const tree = renderer
      .create(<SEO
        language={seoBlog.language}
        title={seoBlog.title}
        description={seoBlog.description}
        image={seoBlog.image}
        keywords={seoBlog.keywords}
        url={seoBlog.url}
        ogtype={seoBlog.ogtype}
        />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  }),

  it("renders all article metatags with proper content", async () => {
    const wrapper = shallow(
      <SEO
        language={seoArticle.language}
        title={seoArticle.title}
        description={seoArticle.description}
        image={seoArticle.image}
        keywords={seoArticle.keywords}
        url={seoArticle.url}
        ogtype={seoArticle.ogtype}
        />)

    expect(wrapper.find("title").text()).toBe(seoArticle.title);

    expect(wrapper.find("meta[name='description']").props().content).toBe(seoArticle.description);
    expect(wrapper.find("meta[name='keywords']").props().content).toBe(seoArticle.keywords);
    expect(wrapper.find("link[rel='canonical']").props().href).toBe(seoArticle.url);

    expect(wrapper.find("meta[name='twitter:title']").props().content).toBe(seoArticle.title + ' | UnaiRoldan.com');
    expect(wrapper.find("meta[name='twitter:description']").props().content).toBe(seoArticle.description);
    expect(wrapper.find("meta[name='twitter:image']").props().content).toBe(seoArticle.image);

    expect(wrapper.find("meta[property='og:type']").props().content).toBe(seoArticle.ogtype);
    expect(wrapper.find("meta[property='og:title']").props().content).toBe(seoArticle.title + ' | UnaiRoldan.com');
    expect(wrapper.find("meta[property='og:url']").props().content).toBe(seoArticle.url);
    expect(wrapper.find("meta[property='og:description']").props().content).toBe(seoArticle.description);
    expect(wrapper.find("meta[property='og:image']").props().content).toBe(seoArticle.image);

  })

})