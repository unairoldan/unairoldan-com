import React from "react"
import renderer from "react-test-renderer"

import Layout from "../../src/components/Layout"
import useSiteMetadata from "../../src/components/SiteMetadata"
import siteData from "../__mocks__/useSiteMetadata.json"

jest.mock('../../src/components/SiteMetadata', () => jest.fn())
useSiteMetadata.mockReturnValue(siteData)

describe("Layout", () => {
  
  it("renders correctly", () => {
    const tree = renderer
      .create(<Layout language="en" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})