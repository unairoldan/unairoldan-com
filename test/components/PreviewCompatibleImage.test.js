import React from "react"
import renderer from "react-test-renderer"

import PreviewCompatibleImage from "../../src/components/PreviewCompatibleImage"
import data from "./__mocks__/images.json"

describe("PreviewCompatibleImage", () => {

  it("renders correctly images", () => {
    const tree = renderer
      .create(<PreviewCompatibleImage imageInfo={{
        image: data.images[0].image,
        alt: `title`,
      }} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  }),

  it("renders correctly images from childImageSharp", () => {
    const tree = renderer
      .create(<PreviewCompatibleImage imageInfo={{
        image: data.images[0].image.childImageSharp,
        alt: `title`,
      }} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it("renders correctly images from string", () => {
    const tree = renderer
      .create(<PreviewCompatibleImage imageInfo={{
        image: data.images[1].image,
        alt: `title`,
      }} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})