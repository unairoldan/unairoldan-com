import React from "react"
import renderer from "react-test-renderer"

import Content from "../../src/components/Content"
import article from "../__mocks__/article.json"

describe("Content", () => {

  it("renders correctly Content", () => {
    const treeContent = renderer
      .create(<Content content={article.markdownRemark.html} />)
      .toJSON()
    expect(treeContent).toMatchSnapshot()
  })
  
})