import React from "react"
import renderer from "react-test-renderer"

import { PureBlogRoll as BlogRoll } from "../../src/components/BlogRoll"
import data from "./__mocks__/blog.json"

describe("BlogRoll", () => {

  it("renders correctly in Spanish", () => {
    const tree = renderer
      .create(<BlogRoll data={data} language="es" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  }),

  it("renders correctly in English", () => {
    const tree = renderer
      .create(<BlogRoll data={data} language="en" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})