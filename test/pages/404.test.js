import React from "react"
import renderer from "react-test-renderer"

import NotFoundPage from "../../src/pages/404"
import useSiteMetadata from "../../src/components/SiteMetadata"
import siteData from "../__mocks__/useSiteMetadata.json"

jest.mock('../../src/components/SiteMetadata', () => jest.fn())
useSiteMetadata.mockReturnValue(siteData)

describe("NotFoundPage", () => {
  
  it("renders correctly", () => {
    const tree = renderer
      .create(<NotFoundPage />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})