import React from "react"
import renderer from "react-test-renderer"

import TagsPage from "../../src/pages/tags/index"
import useSiteMetadata from "../../src/components/SiteMetadata"
import siteData from "../__mocks__/useSiteMetadata.json"
import tags from "./__mocks__/tags.json"

jest.mock('../../src/components/SiteMetadata', () => jest.fn())
useSiteMetadata.mockReturnValue(siteData)

describe("TagsPage", () => {

  it("renders correctly", () => {
    const tree = renderer
      .create(<TagsPage data={tags} />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

})