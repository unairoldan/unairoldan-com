import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'

import SEO from '../components/SEO'
import Header from '../components/Header'
import Layout from '../components/Layout'
import BlogRoll from '../components/BlogRoll'

export const IndexTemplate = ({
  image,
  title,
  language
}) => (
  <div>
    <Header 
      title={title}
      image={image}
      height='300px'
    />
    <section className="section">
      <div className="container">
        <div className="content">
          <BlogRoll
            language={language}
          />
        </div>
      </div>
    </section>
  </div>
)

IndexTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  description: PropTypes.string,
  helmet: PropTypes.object,
}

const IndexPage = ({ data, pageContext }) => {
  const { frontmatter } = data.markdownRemark
  const { siteMetadata } = data.site
  
  return (
    <Layout language={frontmatter.language}>

      <SEO
        language={frontmatter.language}
        title={frontmatter.title}
        description={frontmatter.description}
        image={siteMetadata.siteUrl + frontmatter.image.childImageSharp.fluid.src}
        keywords={frontmatter.keywords}
        url={siteMetadata.siteUrl + pageContext.slug}
        ogtype="blog"
      />

      <IndexTemplate
        title={frontmatter.title}
        image={frontmatter.image}
        language={frontmatter.language}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
    site: PropTypes.shape({
      siteMetadata: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexTemplate($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
        description
        keywords
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        language
      }
    }
    site {
      siteMetadata {
        siteName
        siteUrl
      }
    }
  }
`
