import React from 'react'
import PropTypes from 'prop-types'
import { kebabCase } from 'lodash'
import { graphql, Link } from 'gatsby'
import { JsonLd } from "react-schemaorg";

import SEO from '../components/SEO'
import Layout from '../components/Layout'
import Content from '../components/Content'

import { DiscussionEmbed } from "disqus-react";

export const BlogPostTemplate = ({
  content,
  contentComponent,
  description,
  tags,
  title,
  datestring,
  url,
  language,
  sharethis
}) => {
  const PostContent = contentComponent
  const htmlcontent = content.replace(
    /!#ShareThis/g,
    ShareThis(sharethis, "unairoldan", url, tags, language)
    )
  const disqusConfig = {
    shortname: "unairoldan",
    config: { identifier: title },
  }
  return (
    <section className="section">
      <div className="container content">
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <h1 className="title is-size-2 has-text-weight-bold is-bold-light" >
              {title}
            </h1>
            <p>{description}</p>
            <PostContent content={htmlcontent} />
            <div style={{ marginTop: `2rem` }}>
              <p>
                <span className="has-text-weight-bold is-bold-light">{datestring}</span>
                <span> - </span>
                <span>Unai Roldan</span>
              </p>
              <p style={{ textAlign: 'right' }}>
                <small>UnaiRoldan.com</small>
              </p>
            </div>
            {tags && tags.length ? (
              <div style={{ marginTop: `4rem` }}>
                <h4>Tags</h4>
                <ul className="taglist">
                  {tags.map((tag) => (
                    <li key={tag + `tag`}>
                      <Link to={`/tags/${kebabCase(tag)}/`}>{tag}</Link>
                    </li>
                  ))}
                </ul>
              </div>
            ) : null}
            { datestring  ? (
              <DiscussionEmbed {...disqusConfig} />
            ): null} 
          </div>
        </div>
      </div>
    </section>
  )
}

const ShareThis = (
  text,
  profile,
  url,
  tags,
  language
) => {
  const textLink = text.replace(/ /g, "%20")
  const linkTwitter = `https://twitter.com/intent/tweet?text="${textLink}"&url=${url}&via=${profile}&hashtags=${tags}`
  const linkFacebook = `https://www.facebook.com/sharer/sharer.php?u=${url}&quote="${textLink}"&hashtag=${tags}`
  const linkLinkedin = `https://www.linkedin.com/shareArticle?mini=true&url=${url}&title="${textLink}"&summary="${text}"`
  return `
    <div class="share">
      <p class="title">${ (language === 'en') ? 'Do you like it? Share this:' : '¿Te gusta? Comparte esto:' }</p>
      <div>
        <p class="quote">"${text}"</p>
      </div>
      <div style="text-align: right;" }}>
        <a href=${linkTwitter} class="twitter-share-button"
          target="_blank" rel="noopener noreferrer" aria-label="Share on Twitter">
          <img src="/img/twitter-button.webp" alt="Share on Twitter"
            style="width: 125px; height: 30px;"
          />
        </a>
        <span>&nbsp;</span>
        <a href=${linkFacebook} class="facebook-share-button"
          target="_blank" rel="noopener noreferrer" aria-label="Share on Facebook">
          <img src="/img/facebook-button.webp" alt="Share on Facebook"
            style="width: 125px; height: 30px;"
          />
        </a>
        <span>&nbsp;</span>
        <a href=${linkLinkedin} class="linkedin-share-button"
          target="_blank" rel="noopener noreferrer" aria-label="Share on Linkedin">
          <img src="/img/linkedin-button.webp" alt="Share on Linkedin"
            style="width: 125px; height: 30px;"
          />
        </a>
      </div>
    </div>
  `
};

BlogPostTemplate.propTypes = {
  content: PropTypes.node.isRequired,
  contentComponent: PropTypes.func,
  description: PropTypes.string,
  title: PropTypes.string,
  date: PropTypes.string,
  helmet: PropTypes.object,
}

const BlogPost = ({ data, pageContext }) => {
  const { markdownRemark: post } = data
  const { siteMetadata } = data.site

  return (
    <Layout language={post.frontmatter.language}>

      <SEO
        language={post.frontmatter.language}
        title={post.frontmatter.title}
        description={post.frontmatter.description}
        image={siteMetadata.siteUrl + post.frontmatter.featuredimage.childImageSharp.fluid.src}
        keywords={post.frontmatter.keywords}
        url={siteMetadata.siteUrl + pageContext.slug}
        ogtype="article"
      />

      <JsonLd
        item={{
          '@context': 'https://schema.org',
          '@type': 'NewsArticle',
          mainEntityOfPage: {
            '@type': 'WebPage',
            '@id': siteMetadata.siteUrl
          },
          headline: post.frontmatter.title,
          name: post.frontmatter.name,
          description: post.frontmatter.description,
          image: siteMetadata.siteUrl + post.frontmatter.featuredimage.childImageSharp.fluid.src,
          datePublished: post.frontmatter.date,
          dateModified: post.frontmatter.date,
          keywords: post.frontmatter.keywords.split(", "),
          author: {
            '@type': 'Person',
            'name': 'Unai Roldan'
          },
          publisher: {
            '@type': 'Organization',
            name: 'UnaiRoldan.com',
            logo: {
              '@type': 'ImageObject',
              url: 'https://www.unairoldan.com/img/blog-index-es.webp'
            }
          }
        }}
      />  

      <BlogPostTemplate
        content={post.html}
        contentComponent={Content}
        description={post.frontmatter.description}
        image={siteMetadata.siteUrl + post.frontmatter.featuredimage.childImageSharp.fluid.src}
        tags={post.frontmatter.tags}
        title={post.frontmatter.title}
        url={siteMetadata.siteUrl + pageContext.slug}
        datestring={post.frontmatter.datestring}
        language={post.frontmatter.language}
        sharethis={post.frontmatter.sharethis}
      />
    </Layout>
  )
}

BlogPost.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
  }),
  site: PropTypes.shape({
    siteMetadata: PropTypes.object,
  }),
}

export default BlogPost

export const pageQuery = graphql`
  query BlogPost($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      html
      frontmatter {
        date
        datestring: date(formatString: "MMM DD, YYYY")
        title
        description
        language
        sharethis
        tags
        keywords
        featuredimage {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
    site {
      siteMetadata {
        siteName
        siteUrl
      }
    }
  }
`
