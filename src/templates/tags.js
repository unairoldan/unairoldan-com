import React from 'react'
import { Helmet } from 'react-helmet'
import { Link, graphql } from 'gatsby'
import PreviewCompatibleImage from '../components/PreviewCompatibleImage'

import Header from '../components/Header'
import Layout from '../components/Layout'
import es from '../img/es-flag.svg'
import en from '../img/en-flag.svg'

class TagRoute extends React.Component {
  render() {
    const posts = this.props.data.allMarkdownRemark.edges
    const postLinks = posts.map((post, index) => { 
      const icon = (post.node.frontmatter.language === 'en') ? en : es;
      return (
        <div className="is-parent column is-10" key={index} 
          itemProp="itemListElement" itemScope itemType="https://schema.org/ListItem">
          <article className={`blog-list-item tile is-child box notification`}>
            <header>
              {post.node.frontmatter.featuredimage ? (
                <div className="featured-thumbnail">
                  <PreviewCompatibleImage
                    imageInfo={{
                      image: post.node.frontmatter.featuredimage,
                      alt: `featured image thumbnail for post ${post.node.frontmatter.title}`,
                    }}
                  />
                </div>
              ) : null}
              <p className="post-meta">
                <Link
                  className="title has-text-primary is-size-4"
                  to={post.node.fields.slug}
                  itemProp="item"
                >
                  <span itemProp="name">{post.node.frontmatter.title}</span>
                </Link>
                <span> &bull; </span>
                <span>
                  <img src={icon} alt="idioma" 
                    style={{ width: '16px', height: '16px' }} />
                </span>
                <span className="subtitle is-size-5 is-block">
                  {post.node.frontmatter.date}
                </span>

                {post.node.excerpt}
                <br />
              </p>
            </header>
            <meta itemProp="position" content={index} />
          </article>
        </div>

      )
    })
    const tag = this.props.pageContext.tag
    const siteMetadata = this.props.data.site.siteMetadata
    const totalCount = this.props.data.allMarkdownRemark.totalCount
    const tagHeader = `${totalCount} post${
      totalCount === 1 ? '' : 's'
    } con el tag “${tag}”`

    return (
      <Layout language="es">
        <div>
          <Helmet title={`${tag} | ${siteMetadata.title}`}>
            
            <meta name="description" content={`${siteMetadata.description}`} />
            <link rel="canonical" href={`${siteMetadata.siteUrl + this.props.location.pathname}`}  />

            <meta name="twitter:title" content={`${siteMetadata.title}`} />
            <meta name="twitter:description" content={`${siteMetadata.description}`} />
            <meta name="twitter:image" content={`${siteMetadata.siteUrl + '/img/blog-index-es.webp'}`} />

            <meta property="og:type" content="blog" />
            <meta property="og:title" content={`${siteMetadata.title}`} />
            <meta property="og:url" content={`${siteMetadata.siteUrl + this.props.location.pathname}`}  />
            <meta property="og:description" content={`${siteMetadata.description}`} />
            <meta property="og:image" content={`${siteMetadata.siteUrl + '/img/blog-index-es.webp'}`} />

          </Helmet>

          <Header 
            title={siteMetadata.title}
            image= {siteMetadata.siteUrl + '/img/blog-index-es.webp'}
          />

          <section className="section">
            <div className="container">
              <div className="content">
                <h2 className="title is-size-4 is-bold-light">{tagHeader}</h2>
                <div className="columns is-multiline"
                  itemScope itemType="https://schema.org/BreadcrumbList">
                  {postLinks}
                </div>
                <p>
                  <Link to="/tags/">Explorar todos los tags  /  Browse all tags</Link>
                </p>
              </div>
            </div>
          </section>
        </div>
      </Layout>
    )
  }
}

export default TagRoute

export const tagPageQuery = graphql`
  query TagPage($tag: String) {
    site {
      siteMetadata {
        title
        description
        siteUrl
      }
    }
    allMarkdownRemark(
      limit: 1000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { tags: { in: [$tag] } } }
    ) {
      totalCount
      edges {
        node {
          excerpt(pruneLength: 200)
          id
          fields {
            slug
          }
          frontmatter {
            title
            date(formatString: "MMM DD, YYYY")
            featuredimage {
              childImageSharp {
                fluid(maxWidth: 240, quality: 100) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            language
          }
        }
      }
    }
  }
`
