import React from 'react'

const Header = class extends React.Component {
  render() {
    return (
      <div
        className="full-width-image margin-top-0"
        style={{
          backgroundImage: `url(${
            !!this.props.image.childImageSharp ? this.props.image.childImageSharp.fluid.src : this.props.image
          })`,
          backgroundPosition: `top left`,
          backgroundAttachment: `fixed`,
        }}
      >
        <div
          style={{
            display: 'flex',
            height: '150px',
            lineHeight: '1',
            justifyContent: 'space-around',
            alignItems: 'left',
            flexDirection: 'column',
          }}
        >
          <h1
            className="has-text-weight-bold is-size-3-mobile is-size-2-tablet is-size-1-widescreen"
            style={{
              boxShadow:
                'rgb(214,64,0,.6) 0.5rem 0px 0px, rgb(214,64,0,.6) -0.5rem 0px 0px',
              backgroundColor: 'rgb(214,64,0,.6)',
              color: 'white',
              lineHeight: '1',
              padding: '0.25em',
            }}
          >
            {this.props.title}
          </h1>
        </div>
      </div>
    )
  }
}

export default Header
