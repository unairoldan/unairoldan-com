import React from 'react'
import { Link } from 'gatsby'
import { OutboundLink } from "gatsby-plugin-google-analytics"

import facebook from '../img/social/facebook.svg'
import instagram from '../img/social/instagram.svg'
import twitter from '../img/social/twitter.svg'
import linkedin from '../img/social/linkedin.svg'
import gitlab from '../img/social/gitlab.svg'
import es from '../img/es-flag.svg'
import en from '../img/en-flag.svg'

const Navbar = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      navBarActiveClass: '',
    }
  }

  toggleHamburger = () => {
    // toggle the active boolean in the state
    this.setState(
      {
        active: !this.state.active,
      },
      // after state has been updated,
      () => {
        // set the class in state for the navbar accordingly
        this.state.active
          ? this.setState({
              navBarActiveClass: 'is-active',
            })
          : this.setState({
              navBarActiveClass: '',
            })
      }
    )
  }

  render() {
    return (
      <nav
        className="navbar is-transparent"
        role="navigation"
        aria-label="main-navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            <Link
              to={ (this.props.language === 'en') ? '/en/' : '/' }
              className="navbar-item" title="UnaiRoldan.com">
              <span>UnaiRoldan.com</span>
            </Link>
            {/* Hamburger menu */}
            <div
              className={`navbar-burger burger ${this.state.navBarActiveClass}`}
              data-target="navMenu"
              onClick={() => this.toggleHamburger()}
              onKeyDown={() => this.toggleHamburger()}
              role="button" tabIndex={0}
            >
              <span />
              <span />
              <span />
            </div>
          </div>
          <div
            id="navMenu"
            className={`navbar-menu ${this.state.navBarActiveClass}`}
          >
            <div className="navbar-start has-text-centered">
              <OutboundLink className="navbar-item" href="https://www.slideshare.net/unairoldan">
                { (this.props.language === 'en') ? 'Conferences' : 'Conferencias' }
              </OutboundLink>
              <OutboundLink className="navbar-item" href="https://www.linkedin.com/in/unairoldan">
                { (this.props.language === 'en') ? 'About me' : 'Sobre mi' }
              </OutboundLink>
            </div>
            <div className="navbar-end has-text-centered">
            <Link 
                to="/"
                className="navbar-item">
                <span className="icon">
                  <img src={es} alt="Spanish" />
                </span>
              </Link>
              <Link 
                to="/en"
                className="navbar-item">
                <span className="icon">
                  <img src={en} alt="English" />
                </span>
              </Link>
              <span className="social">&nbsp;</span>
              <OutboundLink
                title="linkedin"
                className="navbar-item"
                href="https://www.linkedin.com/in/unairoldan"
                target="_blank" rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={linkedin} alt="Linkedin" />
                </span>
              </OutboundLink>
              <OutboundLink
                title="twitter"
                className="navbar-item"
                href="https://twitter.com/unairoldan"
                target="_blank" rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={twitter} alt="Twitter" />
                </span>
              </OutboundLink>
              <OutboundLink
                title="facebook"
                className="navbar-item"
                href="https://www.facebook.com/unairoldan"
                target="_blank" rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={facebook} alt="Facebook" />
                </span>
              </OutboundLink>
              <OutboundLink
                title="instagram"
                className="navbar-item"
                href="https://www.instagram.com/unairoldan"
                target="_blank" rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={instagram} alt="Instagram" />
                </span>
              </OutboundLink>
              <OutboundLink
                title="gitlab"
                className="navbar-item"
                href="https://www.gitlab.com/unairoldan"
                target="_blank" rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={gitlab} alt="GitLab" />
                </span>
              </OutboundLink>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
