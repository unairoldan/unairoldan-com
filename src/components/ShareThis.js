import React from 'react'
import PropTypes from 'prop-types'

const ShareThis = ({
  text,
  profile,
  url,
  tags,
  language
}) => {
  const linkTwitter = `https://twitter.com/intent/tweet?text="${text}"&url=${url}&via=${profile}&hashtags=${tags}`
  const linkFacebook = `https://www.facebook.com/sharer/sharer.php?u=${url}&quote="${text}"&hashtag=${tags}`
  const linkLinkedin = `https://www.linkedin.com/shareArticle?mini=true&url=${url}&title="${text}"&summary="${text}"`
  return (
    <div className="share">
      <p className="title">{ (language === 'en') ? 'Do you like it? Share this:' : '¿Te gusta? Comparte esto:' }</p>
      <div>
        <p className="quote">"{text}"</p>
      </div>
      <div style={{ textAlign: 'right' }}>
        <a href={linkTwitter} className="twitter-share-button"
          target="_blank" rel="noopener noreferrer" aria-label="Share on Twitter">
          <img src="/img/twitter-button.webp" alt="Share on Twitter"
            style={{
              width: '125px',
              height: '30px'
            }}
          />
        </a>
        <span>&nbsp;</span>
        <a href={linkFacebook} className="facebook-share-button"
          target="_blank" rel="noopener noreferrer" aria-label="Share on Facebook">
          <img src="/img/facebook-button.webp" alt="Share on Facebook"
            style={{
              width: '125px',
              height: '30px'
            }}
          />
        </a>
        <span>&nbsp;</span>
        <a href={linkLinkedin} className="linkedin-share-button"
          target="_blank" rel="noopener noreferrer" aria-label="Share on Linkedin">
          <img src="/img/linkedin-button.webp" alt="Share on Linkedin"
            style={{
              width: '125px',
              height: '30px'
            }}
          />
        </a>
      </div>
    </div>
  )
}

ShareThis.propTypes = {
  text: PropTypes.string,
  profile: PropTypes.string,
  url: PropTypes.string,
}

export default ShareThis
