import React from 'react'
import { Helmet } from 'react-helmet'
import { withPrefix } from 'gatsby'
import CookieConsent from "react-cookie-consent";
import { JsonLd } from "react-schemaorg";
import './all.sass'

import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import useSiteMetadata from './SiteMetadata'

const TemplateWrapper = ({ children, language }) => {
  const { siteUrl, siteName } = useSiteMetadata()
  return (
    <div>

      <Helmet>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta name="robots" content="all" />

        <link rel="apple-touch-icon" sizes="180x180" href={`${withPrefix('/')}img/apple-touch-icon.png`} />
        <link rel="icon" type="image/png" href={`${withPrefix('/')}img/favicon-32x32.png`} sizes="32x32" />
        <link rel="icon" type="image/png" href={`${withPrefix('/')}img/favicon-16x16.png`} sizes="16x16" />
        <link rel="mask-icon" href={`${withPrefix('/')}img/safari-pinned-tab.svg`} color="#5bbad5" />
        <link rel="manifest" href={`${withPrefix('/')}site.webmanifest`} />
        <meta name="msapplication-TileColor" content="#dddddd" />
        <meta name="theme-color" content="#fff" />

        <meta property="og:url" content={siteUrl} />
        <meta property="og:site_name" content={siteName} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@unairoldan" />
        <meta name="twitter:creator" content="@unairoldan" />

      </Helmet>

      <JsonLd
        item={{
          '@context': 'https://schema.org',
          '@type': 'Person',
          name: 'Unai Roldan',
          gender: 'https://schema.org/Male',
          brand: 'UnaiRoldan.com',
          nationality: 'Spain',
          url: siteUrl,
          image: 'https://www.unairoldan.com/img/blog-index-es.webp',
          sameAs: [
              'https://www.twitter.com/unairoldan',
              'https://www.facebook.com/unairoldan',
              'https://www.instagram.com/unairoldan',
              'https://www.linkedin.com/in/unairoldan/',
              'https://www.slideshare.net/unairoldan',
              'https://www.gitlab.com/unairoldan',
          ]
        }}
      />

      <Navbar language={language} />
      <div>{children}</div>
      <Footer language={language} />

      <CookieConsent
        location="bottom"
        buttonText="Ok"
        cookieName="UnaiRoldan.com-gdpr-google-analytics"
        style={{ background: "#EEEEEE", color: "#222222" }}
        buttonStyle={{ background: "#CCCCCC", color: "#333333", fontSize: "12px" }}
        expires={150}
      >
        Este sitio web utiliza cookies para mejorar la experiencia del usuario - This website uses cookies to enhance the user experience{" "}
      </CookieConsent>

    </div>
  )
}

export default TemplateWrapper
