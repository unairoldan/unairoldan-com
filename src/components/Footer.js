import React from 'react'
import { Link } from 'gatsby'
import { OutboundLink } from "gatsby-plugin-google-analytics"

import facebook from '../img/social/facebook.svg'
import instagram from '../img/social/instagram.svg'
import twitter from '../img/social/twitter.svg'
import linkedin from '../img/social/linkedin.svg'
import gitlab from '../img/social/gitlab.svg'

const Footer = class extends React.Component {
  render() {
    return (
      <footer className="footer has-background-black has-text-white-ter">
        <div className="content has-text-centered has-background-black has-text-white-ter">
          <div className="container has-background-black has-text-white-ter">
            <div className="columns">
              <div className="column is-4">
                <section className="menu">
                  <ul className="menu-list">
                    <li>
                      <Link 
                        to={ (this.props.language === 'en') ? '/en/' : '/' }
                        className="navbar-item">
                        UnaiRoldan.com
                      </Link>
                    </li>
                    <li>
                      <a className="navbar-item" href="https://www.slideshare.net/unairoldan">
                        { (this.props.language === 'en') ? 'Conferences' : 'Conferencias' }
                      </a>
                    </li>
                    <li>
                      <a className="navbar-item" href="https://www.linkedin.com/in/unairoldan">
                        { (this.props.language === 'en') ? 'About me' : 'Sobre mi' }
                      </a>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4 social">
                <OutboundLink title="linkedin" href="https://www.linkedin.com/in/unairoldan" 
                  target="_blank" rel="noopener noreferrer">
                  <img
                    src={linkedin}
                    alt="Linkedin"
                    style={{ width: '1em', height: '1em' }}
                  />
                </OutboundLink>
                <OutboundLink title="twitter" href="https://twitter.com/unairoldan" 
                  target="_blank" rel="noopener noreferrer">
                  <img
                    className="fas fa-lg"
                    src={twitter}
                    alt="Twitter"
                    style={{ width: '1em', height: '1em' }}
                  />
                </OutboundLink>
                <OutboundLink title="facebook" href="https://www.facebook.com/unairoldan" 
                  target="_blank" rel="noopener noreferrer">
                  <img
                    src={facebook}
                    alt="Facebook"
                    style={{ width: '1em', height: '1em' }}
                  />
                </OutboundLink>
                <OutboundLink title="instagram" href="https://www.instagram.com/unairoldan" 
                  target="_blank" rel="noopener noreferrer">
                  <img
                    src={instagram}
                    alt="Instagram"
                    style={{ width: '1em', height: '1em' }}
                  />
                </OutboundLink>
                <OutboundLink title="gitlab" href="https://www.gitlab.com/unairoldan" 
                  target="_blank" rel="noopener noreferrer">
                  <img
                    src={gitlab}
                    alt="GitLab"
                    style={{ width: '1em', height: '1em' }}
                  />
                </OutboundLink>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
