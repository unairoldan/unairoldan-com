import React from "react"
import { Helmet } from "react-helmet"

const SEO = ({
  title,
  description,
  keywords,
  url,
  image,
  ogtype,
  language,
}) => {
  
  const ogtitle = (ogtype === "blog") ? 'UnaiRoldan.com - ' + title : title + ' | UnaiRoldan.com'

  return (
    <Helmet
    titleTemplate={ ogtype === "blog" ? 'UnaiRoldan.com - %s' : '%s | UnaiRoldan.com' }
    htmlAttributes={{ lang : language === "en" ? 'en' : 'es' }}
    >

        <title>{`${title}`}</title>
        <meta name="description" content={`${description}`} />
        <meta name="keywords" content={`${keywords}`} />
        <link rel="canonical" href={`${url}`}  />

        <meta name="twitter:title" content={`${ogtitle}`} />
        <meta name="twitter:description" content={`${description}`} />
        <meta name="twitter:image" content={`${image}`} />

        <meta property="og:type" content={`${ogtype}`} />
        <meta property="og:title" content={`${ogtitle}`} />
        <meta property="og:url" content={`${url}`} />
        <meta property="og:description" content={`${description}`} />
        <meta property="og:image" content={`${image}`} />

    </Helmet>
  )
}
  
export default SEO