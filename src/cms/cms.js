import CMS from 'netlify-cms-app'
import { es } from 'netlify-cms-locales';

import BlogPostPreview from './preview-templates/BlogPostPreview'

CMS.registerLocale('es', es);
CMS.registerPreviewTemplate('blog', BlogPostPreview)
