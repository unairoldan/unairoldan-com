---
templateKey: blog-post
title: Agile Retrospective (with Retromat)
date: 2016-08-28T10:00:00.000Z
description: >-
  During the agile retrospective, also known as "Retro", the team reflects on
  what happened in the iteration and identify actions to focus on the continuous
  improvement.
featuredpost: false
sharethis: During the Retrospective the team reflects on what happened in the iteration and identify actions to focus on the continuous improvement
language: en
featuredimage: /img/agile-retrospective.webp
keywords: scrum, retrospective, agile, meeting, continuous improvement, improvement, agile retrospective, set the stage, gather data, generate insights, decide what to do, close, retromat
tags:
  - scrum
  - retrospective
---
In those meetings, identified as one of the ceremonies of Scrum framework, and timeboxed to three hours or less, the team reflects about the daily work of the team itself, both qualitative and quantitative ways, endeavoring to uncover what is working well, what isn’t, and what can the team do better the next time.

![Agile Retrospective](/img/agile-retrospective.webp "Agile Retrospective")

The quantitative side of the agile retrospective is relatively easy to address and understand, provided that we have access to data about work and team performance. Metrics about team velocity, value delivered, defect density or even data about team’ happiness are really relevant in every day life of good teams, and may provide clues to proposing improvement actions. Having a history of all the data that we consider relevant as a team we can make comparisons and identify trends and breaking points that have affected productivity, and we will obtain conclusions about what has been happening in the team over the time.

Thus, the quantitative side of an Agile retrospective seems a simple task, but it is not the case of the qualitative side…

## Agile Retrospective

Achieving an open and trusting environment inside the team to facilitate for the people to open his mind and be completely sincere and honest when doing introspection is not an easy task, and this is the main reason why it the qualitative part of an agile retrospective is not an easy task.

In order to have an effective agile retrospective and to encourage the team to build on successes and address problems, this meeting has a recommended structure, popularized by [Esther Derby](https://twitter.com/estherderby) and [Diana Larsen](https://twitter.com/DianaOfPortland) in their book Agile Retrospectives, that facilitate the creation of this trust and open environment.

This structure has five steps, each with a specific goal:

1. **Set the stage:** Set the goal, set the time and give people time to “arrive” and get into the right mood.
2. **Gather data:** Create a “shared memory” with information, points of view and even emotions of every member of the team.
3. **Generate insights:** Discover what this information means. Why did it happen?. Identify possible patterns and see the big picture.
4. **Decide what to do:** Create a concrete plan with actions or experiments of how you will address the identified impediments.
5. **Close:** Summarize and close clearly the agile retrospective, reviewing plan and gathering feedback. How could the agile retrospective improve?

Sometimes is difficult to design powerful and effective agile retrospectives, above all if we work with several teams the same time, since design the five steps usually take some time. In order to facilitate this task [Corinna Baldauf](https://twitter.com/findingmarbles) has created Retromat, a wonderful collection of ideas and activities that facilitate the creation of agile retrospectives.

!#ShareThis

## Retromat

[Retromat](http://plans-for-retrospectives.com/), also known as Ret-O-Mat, is a collection of ideas and activities to facilitate the design of agile retrospectives, and it can be used in both online or [printed-version](http://plans-for-retrospectives.com/books.html) way. It covers the five steps of the traditional structure of an agile retrospective, and permits you browse to select different activities for each step, or create a random combination.

![Retromat](/img/retrospective-retromat.webp "Retromat")

With Ret-O-Mat you can design your retrospectives easily by selecting one of the proposed activities for each step of agile retrospective.

## Reading about

Some books and useful references about agile retrospective:

* **Agile Retrospectives: Making Good Teams Great** Esther Derby, Diana Larsen
* **Getting Value out of Agile Retrospectives: A Toolbox of Retrospective Exercises** Luis Gonçalves, Ben Linders
* **The Art of Facilitation: The Essentials for Leading Great Meetings and Creating Group Synergy** Dale Hunter

If you need more information, here you have other posts or presentations about agile retrospectives:

* [Agile Retrospective Resource Wiki](http://retrospectivewiki.org/)
* [Las 5 etapas de una retrospectiva](http://www.dosideas.com/noticias/retrospectivas/876-las-5-etapas-de-una-retrospectiva.html)
* [Técnicas para la realización de Retrospectivas](http://media.kleer.la/kleer-tecnicas-de-retrospectivas-es.pdf)

More in [Linkedin](https://www.linkedin.com/pulse/agile-retrospective-retromat-unai-roldán/)
