---
templateKey: blog-post
title: Retrospectiva Agile (con Retromat)
date: 2016-08-28T10:00:00.000Z
description: >-
  Durante la retrospectiva agile, también conocida como "Retro", el equipo
  reflexiona sobre lo que ha ocurrido durante la anterior iteración e identifica
  acciones para enfocarse en la mejora continua.
featuredpost: false
sharethis: Durante la Retrospectiva el equipo reflexiona sobre lo que ha ocurrido durante la anterior iteración e identifica acciones para enfocarse en la mejora continua
language: es
featuredimage: /img/retrospectiva-agile.webp
keywords: agile, restrospectiva, reuniones, mejora continua, scrum, mejora, retrospectiva agile, preparar el escenario, recabar datos, generar entendimiento, decidir que hacer, cierre, retromat
tags:
  - scrum
  - retrospectiva
---
Dentro de esta reunión, identificada como una de las ceremonias de Scrum, y que tiene un timebox máximo de tres horas, el equipo reflexiona sobre el trabajo del propio equipo a nivel cualitativo y cuantitativo, tratando de descubrir que está funcionando bien, que no, y que podemos hacer mejor en el futuro.

![Retrospectiva Agile](/img/retrospectiva-agile.webp "Retrospectiva Agile")

La parte cuantitativa de una retrospectiva agile es relativamente sencilla de abordar y de entender, siempre y cuando tengamos acceso a datos sobre trabajo y el desempeño del equipo. Mediciones de la velocidad del equipo, valor entregado, densidad de defectos o incluso métricas sobre la felicidad del equipo son muy relevantes en el día a día de los buenos equipos agile, y pueden darnos pistas a la hora de proponer acciones de mejora. Teniendo un histórico de todos los datos que como equipo consideremos relevantes, podemos hacer comparaciones y detectar tendencias o puntos de inflexión que hayan afectado a la productividad, y conseguiremos extraer conclusiones mas o menos claras de lo que ha ido ocurriendo en el equipo a lo largo del tiempo.

Por lo tanto, la parte cuantitativa de una retrospectiva agile parece una tarea sencilla, pero no es el caso de la parte cualitativa...

## Retrospectiva Agile

Conseguir generar un espacio de confianza dentro del equipo, que permita a las personas que lo integran abrir su mente y ser totalmente sinceros y honestos cuando hacemos introspección no es algo fácil de lograr. Y esta es la principal razón por la que facilitar una retrospectiva agile, o al menos lo que hemos llamado la "parte cualitativa", no es una tarea sencilla.

Con el objetivo de tener retrospectivas agile efectivas, y para ayudar al equipo a construir sobre los éxitos y afrontar los problemas, la reunión tiene una estructura recomendada, popularizada por [Esther Derby](https://twitter.com/estherderby) y [Diana Larsen](https://twitter.com/DianaOfPortland) en su libro Agile Retrospectives, que facilita la creación de este espacio abierto y de confianza.

Esta estructura tiene cinco pasos, cada uno de ellos con un objetivo especifico:

1. **Preparar el escenario:** Establecer la meta, marcar tiempo objetivo y darle tiempo a los miembros del equipo para "aterrizar" en la retrospectiva con el estado de ánimo adecuado.
2. **Recabar datos:** Crear una "memoria compartida" con la información, puntos de vista e incluso las emociones de cada uno de los integrantes del equipo.
3. **Generar entendimiento:** Descubrir que significa esta información. ¿Por qué sucede? Identificar posibles patrones y lograr tener una visión completa.
4. **Decidir que hacer:** Crear un plan concreto con acciones o experimentos sobre como abordar los impedimentos identificados.
5. **Cierre:** Resumir y cerrar claramente la retrospectiva agile, revisando el plan de acciones y recabando feedback de la sesión.

A veces es difícil diseñar y facilitar una retrospectiva agile de forma efectiva y eficaz, sobre todo cuando trabajamos con varios equipos al mismo tiempo y no tenemos todo el tiempo deseable para diseñar y preparar la sesión. Con el objetivo de facilitar esta tarea, [Corinna Baldauf](https://twitter.com/findingmarbles) ha creado Retromat, una maravillosa colección de ideas y dinámicas para facilitar el diseño de la retrospectiva agile.

!#ShareThis

## Retromat

[Retromat](http://plans-for-retrospectives.com/), también conocido como Retr-O-Mat, es una colección de ideas y dinámicas para facilitar el diseño de retrospectivas agile, y puede ser utilizado tanto en su versión on-line como [su versión impresa](http://plans-for-retrospectives.com/books.html). Cubre las cinco fases de la estructura tradicional de una retrospectiva agile, y te permite explorar alternativas y seleccionar diferentes actividades para cada paso, o crear una combinación aleatoria.

![Retromat](/img/retrospective-retromat.webp "Retromat")

Con Ret-O-Mat puedes diseñar tu retrospectiva agile de forma sencilla seleccionando una de las actividades propuestas para cada uno de los pasos de la ceremonia.

## Bibliografía

Algunos libros o referencias útiles sobre retrospectivas agile:

* **Agile Retrospectives: Making Good Teams Great** Esther Derby, Diana Larsen
* **Getting Value out of Agile Retrospectives: A Toolbox of Retrospective Exercises** Luis Gonçalves, Ben Linders
* **The Art of Facilitation: The Essentials for Leading Great Meetings and Creating Group Synergy** Dale Hunter

Si necesitas mas información, aquí tienes otros entradas en blogs o presentaciones sobre la retrospectiva agile:

* [Agile Retrospective Resource Wiki](http://retrospectivewiki.org/)
* [Las 5 etapas de una retrospectiva](http://www.dosideas.com/noticias/retrospectivas/876-las-5-etapas-de-una-retrospectiva.html)
* [Técnicas para la realización de Retrospectivas](http://media.kleer.la/kleer-tecnicas-de-retrospectivas-es.pdf)

Más en [Linkedin](https://www.linkedin.com/pulse/agile-retrospective-retromat-unai-roldán/)
