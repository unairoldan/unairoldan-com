---
templateKey: blog-post
title: Digital Transformation of the second Spanish Bank
date: 2017-02-25T11:00:00.000Z
description: >-
  Because it's 2015. This was the answer of Justin Trudeau, Canadian premier
  minister, to a journalist who asked him about the importance for him of having
  a cabinet with gender balance (similar number of men and women). Although for
  some people this could be a question difficult to argue, the answer was
  obvious. There is not much else to explain.
featuredpost: true
sharethis: After a year and a few months of digital transformation, the bank reported a 10.9 percent increase in revenues, reaching € 23.68 billion, with the digital banking program leading the results
language: en
featuredimage: /img/digital-banking.webp
keywords: strategy, organization, business agility, banking, digital transformation, agile@scale, safe, less, nexus
tags:
  - business agility
  - strategy
  - organization
  - operating model
  - change management
  - agile
  - scrum
  - human resources
---
**The same thing happens today with the need of change in large organizations. The answer is obvious, there is not much to explain.** And perhaps this is the year in which all the large organizations are leaping into the abyss of Digital Transformation, or rather the ones that are missing, because many of them have already started their path with greater or lesser success. **Sometimes without knowing what a Digital Transformation process really means**. What it implies.

Change bells are ringing, and unlike a few years ago, now everyone is listening.

![digital transformation](/img/digital-banking.webp "digital transformation")

The year began with the article written by Mary Poppendieck titled The End of Enterprise IT, describing some aspects of the change at organizational level at ING, the Dutch bank that among other countries also operates in Spain. It tells **how the organization empirically discovered that this change requires reinventing itself at structural, hierarchical and business level**, and that it would implicate, of course, the consequent **cultural change** in the company. Organizations have already discovered that they are late in this topic, they should have started this kind of Organizational transformations three or more years ago, but right now some of them are ready (not all), so everything has been conveniently baptized at market level with the label Digital Transformation, which maybe is not the most correct name, but Business is Business.

Much has been written and said about the transformation of large organizations: it is impossible, they do not want to change, it is easy, I do it in 20 days, it is not worth it, etc. Curiously, many of these phrases are usually from people who have never been part of a truly transformation process, **involving all levels of the organization and not just working with some technology teams** to get them doing Scrum from its' silo on the 2nd floor of one of the organization's building.

Due to all this, in my case I only want **to tell an experience**, joining concrete data and context, gathering information that has already been published and putting everything together from the day by day experience of being part of this digital transformation. Of course, giving a special touch and writing it from the affection that I have to this project and the wonderful professionals who have been and are part of it.

## The beginning at digital banking

It looked like a normal Thursday in May, but morning coffee was not going to be like the other days; In fact, there were many coffees that morning. Digital Bank woke up with the news of the year, and perhaps of the decade for our division: **Carlos Torres, executive director of Digital Bank, [had been chosen to be the new CEO of the Bank](https://www.wsj.com/articles/spains-bbva-appoints-new-president-1430757128)**. It would not become official until Monday, but at this point, after so many months of work, we already intuited what this could mean: **The digital transformation was no longer just digital to become the new strategic commitment of the organization**. The transformation of [the entire ecosystem](http://www.elconfidencialdigital.com/dinero/CEO-BBVA-Google-Amazon-Apple_0_2484351723.html).

It had all started at the end of the summer of 2014, when BBVA decided [to leap to the digital world](http://www.eldiario.es/edcreativo/blogs/clickbanking/metodologia-agile-scrum-bbva-fintech_6_522207788.html), and to this aim it started a project, with 150 people, **focused on providing new digital capabilities and starting cultural change within the organization**. The commitment was clear, and the 12 teams that started this new road were accompanied by a different team who will help the organization to mutate to achieve its' goals, the Agile Coaches team, Change Agents or \[insert here the name that you like]. And I had (and I have) the luck to be part of that [team](https://twitter.com/USTAgile/lists/coaches-team/members).

After a very agile start, everything went quickly and more or less structured. We began working with the **digital product teams to have a two-week cadence**, laying the foundations at a methodological level (almost every team had decided to use [Scrum](/tags/scrum/)) and we started working strongly the initial training **to gradually discover with what role every person felt comfortable with** - obviously working from a systemic vision. Once set the bases, and **work at the Portfolio Management level** began to take shape, new teams started arriving to the Digital Bank portfolio. Teams which obviously brought under the arm their products, **reaching a total of 20 teams six months after starting the transformation in the area**. Among these teams was habitual the ones linked to banking products, [although there were also exceptions](http://www.expansion.com/economia-digital/companias/2015/12/22/56795576268e3ea2388b471f.html), and all of these teams were compound of people specialized in **business, technology and from other *[enabler areas](https://theitriskmanager.wordpress.com/2016/12/03/the-real-state-of-scaled-agile/)* in the cases where it was necessary**.

!#ShareThis

At this point, it was time to work in the **organizational design to ensure coordination and alignment**, and it was important to include teams from relevant areas such as **Risks**, **Security** or **Legal and Compliance**, as well as start working with **HR** for issues such as creation of new roles or the new career paths. Once the business programs were structured, some new teams incorporated to digital bank area, **being in total already 32 product teams and reaching a figure of [+550 people in less than a year](https://www.finextra.com/news/fullstory.aspx?newsitemid=28358) of cultural and digital transformation**. All within the same Product Portfolio, the Digital Banking, but it was not the only one. For months, the same initiative was being worked in other areas of the organization, with other teams of coaches deployed working along the same lines. Those included different verticals of business with very different operating models: from innovation areas and digital product to investment banking, even*enabler*areas such as legal audit teams.

A year and three months after starting this new challenge, **the initiative was already a reality in several areas of the same organization**, involving many people who **worked in a coordinated way, delivering products to market every three months at most** and providing their customers new services and digital channels to make their daily life easier. Are you a BBVA customer? Have you perceived it? It is not a chimera, it can be done, and it has been made.

## Digital transformation impact

Let's be honest, nobody cares if all your teams ~~do~~ work with [Scrum](/tags/scrum/), if they have a Product Backlog or if the work of your Global Front team[is exposed by Google at its annual event in Europe](https://twitter.com/cellsjs/status/734663578041602048). No one. Does not even matter if your [Digital Marketing team is the most Agile team in the Bank](https://www.sistrix.es/blog/bbva-transformacion-digital-google/). Don't get me wrong, all this is very well, and are important victories to celebrate, you are on the right track... but what will truly measure the success of your organizational change are the results at business level. **The impact on your income statement**.

After a year and a few months of digital transformation, **the bank reported a 10.9% increase in revenues, reaching € 23.68 billion**, with [the digital banking program leading the results](https://www.finextra.com/newsarticle/28421/digital-transformation-driving-earnings-at-bbva). In addition to this, **14.8 million customers already operated with the bank through web and mobile banking**, being this the main channel for 8.5 million customers, almost the double from the previous year. But it not was just about economic results; All the transformation metrics had a very significant improvement, especially those associated with delivery cycles and customer satisfaction .Even the evolution of the IReNe (Net Recommendation Index, similar to [Net Promoter Score](https://en.wikipedia.org/wiki/Net_Promoter)) allowed the bank to climb and overcome its main competitors.

The bank[showed its' Transformation Journey](https://accionistaseinversores.bbva.com/TLBB/micros/bbvain2015/en/), and **the results in the income statement supported the strategic bet**. So much so far, that was that moment when we realized that this journey had just only begun... new horizons were opened, far away, far far away in some cases.

This was how the **global transformation** of Spain's second largest bank started... **that is also the sixth largest in Latin America**.

More in Linkedin: [https://www.linkedin.com/pulse/digital-transformation-second-spanish-bank-unai-roldán/](https://www.linkedin.com/pulse/digital-transformation-second-spanish-bank-unai-rold%C3%A1n/)
