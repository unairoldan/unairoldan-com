---
templateKey: blog-post
title: Comparativa de Certificación Scrum Master. ¿Cual debería elegir?
date: 2016-09-12T10:00:00.000Z
description: >-
  Desde mis comienzos con Agile hace ya algunos años son muchas las preguntas
  que la gente me traslada en mi trabajo con diferentes clientes, en las
  reuniones de la comunidad o en mi día a día, pero hay una pregunta que siempre
  sobresale por encima del resto, y que parece cada día mas difícil de responder
  para los no iniciados: ¿Cual es la mejor certificación Scrum Master disponible
  en el mercado?
featuredpost: true
sharethis: CSM y PSM I son los programas de certificación Scrum Master más reconocidos a nivel mundial, pero PMI-ACP no está lejos de ambas
language: es
featuredimage: /img/scrum-cubes.webp
keywords: agile, scrum, certificacion, psm, psm i, csm, scrum study, european scrum, scrum.org, scrum alliance, scrum master, certificación scrum master
tags:
  - agile
  - scrum
  - certificacion
  - product management
  - formación
---
Dada la explosión que vivimos en los últimos años con todo lo relacionado con metodologías ágiles, ya no solo a nivel de desarrollo de software en pequeñas empresas de nicho o start-ups, sino también a nivel de [grandes organizaciones](/tags/business-agility/), no es algo casual ver como aparecen muchas empresas dispuestas a ser proveedoras de certificaciones de Scrum Master. Empresas que no necesariamente cuentan con una trayectoria en el mundo [Agile](/tags/agile/), y que tampoco están respaldadas por entidades reconocidas, pero que sí han creado nuevos programas de certificación que cuentan con nombres llamativos, difícilmente diferenciables, y todos ellos escritos como acrónimos de tres letras, lo que hace que muchas veces sea ciertamente difícil diferenciar entre ellas.

![Scrum Master Certification](/img/scrum-cubes.webp "Scrum Master Certification")

Posiblemente sea sencillo hacer esta distinción entre los diferentes programas de certificación Scrum Master a las personas involucradas en la comunidad Agile, pero no es una tarea sencilla para aquellos que acaban de llegar a ella. Y aunque no existe una respuesta directa a la pregunta de cual certificación es la mejor, ya que depende de las preferencias y necesidades puntuales de cada persona, si es cierto que existen programas de certificación ampliamente reconocidos en la industria que parten con mucha ventaja a la hora de responder.

## Una pequeña introducción histórica
Como es habitual, todos los programas de certificación son creados y soportados por organizaciones dedicadas a ello, y en el caso de la certificación Scrum Master fue la [Scrum Alliance](http://www.scrumalliance.org/), fundada por [Ken Schwaber](https://twitter.com/kschwaber), [Mike Cohn](https://twitter.com/mikewcohn) y [Esther Derby](https://twitter.com/estherderby) la primera en ofrecer el programa de certificación en el año 2002.

[Scrum Alliance](http://www.scrumalliance.org/) fue la principal (y única) proveedora de cursos de certificación hasta el año 2009, [cuando Ken Schwaber sale de la organización para fundar Scrum.org](http://www.scrum.org/About/Origins), liberando pocos meses más tarde su propia certificación Scrum Master al mercado.

Poco después, el [Project Management Intitute (PMI)](http://www.pmi.org/), que históricamente había renegado de los métodos ágiles realiza un importante cambio de rumbo incluyendo a las metodologías ágiles en la versión 5 del PMBoK, su marco de referencia para la gestión de proyectos, y creando su propia certificación Agile. Fue tras este movimiento del PMI cuando comenzaron a aparecer nuevas organizaciones con nuevas certificaciones Scrum Master bajo el brazo, llegando a crear incluso alguna de ellas su propio marco metodológico.

## ¿Cual es la mejor Certificación Scrum Master?
Para empezar delimitando el alcance la pregunta, estas son algunas de las certificaciones existentes a día de hoy en el mercado, siendo las tres primeras las mas reconocidas a nivel global, que luego compararemos en detalle:

- Professional Scrum Master (PSM I) - Scrum.org
- Certified Scrum Master (CSM) - ScrumAlliance
- Agile Certified Practitioner (PMI-ACP) - PMI
- Agile Scrum Master (ASM) - EXIN
- Scrum Master Certified (SCM) - Scrum Study

Todas ellas están soportadas por diferentes organizaciones y cuentan con diferentes programas de certificación relacionadas con Agile, no solo para Scrum Masters, siendo la [Scrum Alliance](http://www.scrumalliance.org/) y [Scrum.org](http://www.scrum.org/) las organizaciones mas ampliamente reconocidas dentro del mundo Agile.

!#ShareThis

### Professional Scrum Master I (PSM I)
Professional Scrum Master I es la certificación Scrum Master de referencia en Scrum.org, siendo para mi gusto la mas completa de todas las disponibles en el mercado. Existe la posibilidad de acceder directamente al examen de certificación con un precio de 150$ o a través de una formación de dos días que incluye la cuota del examen.

Cuenta con varios niveles de certificación, pudiéndose acceder a la certificación PSM II y PSM III a medida que consigues la acreditación inmediatamente anterior.

[Professional Scrum Master I (PSM I)](https://www.scrum.org/Assessments/Professional-Scrum-Master-Assessments/PSM-I-Assessment), [Scrum.org](http://www.scrum.org/)

### Certified Scrum Master (CSM)
Certified Scrum Master es la certificación Scrum Master sin duda mas extendida a nivel mundial y la que cuenta con más formadores, y por ende, más cursos disponibles. A diferencia de la PSM I de la Scrum.org, para obtener la certificación CSM es obligatorio asistir a una formación de 2 días, siendo por ello el coste de adquisición mas alto.

Además de esto, es necesario renovar la certificación cada dos años, pagando una cuota de 100$ sin realizar ningún tipo de formación adicional o examen.

[Certified Scrum Master (CSM)](http://www.scrumalliance.org/certifications/practitioners/certified-scrummaster-csm), [Scrum Alliance](http://www.scrumalliance.org/)

### Agile Certified Practitioner (PMI-ACP)
Agile Certified Practitioner es la certificación Scrum Master del Project Management Institute, y abarca no solo el framework Scrum sino otros métodos agile como Kanban, Lean o eXtreme Programming. Pese a ser una certificación Agile, el modelo de certificación y el tipo de examen es muy similar al clásico PMP, muy extenso y centrado en la teoría.

Al igual que su certificación hermana PMP, la certificación PMI-ACP tiene unos requisitos mínimos para poder acceder a ella: es necesario acreditar al menos 2000h de experiencia en proyectos, 1500h trabajando en proyectos Agile y al menos 21 horas de formación en prácticas ágiles.

[Agile Certified Practitioner (PMI-ACP)](http://www.pmi.org/certifications/types/agile-acp), [PMI](http://www.pmi.org/)

### EXIN Agile Scrum Master (ASM)
Entramos en terrenos oscuros con la certificación Scrum Master de EXIN, la conocida compañía holandesa dedicada a proveer servicios de certificación IT a lo largo y ancho del mundo. Según su página web, la certificación ha sido desarrollada con expertos de todo el mundo, pero no nombra ninguno de ellos. Es una certificación que, más allá de empresas que solo buscan el sello de una certificación, no está reconocida en la industria.

El precio del examen es de 170€ y tienes que responder correctamente 26 de las 40 preguntas del test para aprobar, un 65%.

[Agile Scrum Master (ASM)](http://www.exin.com/NL/en/certifications/&exam=exin-agile-scrum-master), [EXIN](http://www.exin.com/)

### Scrum Master Certified (SCM)
Otra de las certificaciones aparecidas en los últimos tiempos es la SCM de Scrum Study, que forma parte de VMEdu. El precio del examen es de 450$ y está disponible en varios idiomas, teniendo incluso la posibilidad de realizar un curso con ellos. En su web no hay información sobre la dificultad de pasar el examen, pero si hacen gala de un 95% de tasa de aprobados.

Al igual que la certificación de EXIN, a día de hoy la certificación Scrum Master SCM de Scrum Study no está reconocida en la industria mas allá de compañías que solo buscan tener "un Scrum Master certificado".

[Scrum Master Certified (SCM)](http://www.scrumstudy.com/scrum-master-certification-training.asp), [Scrum Study](http://www.scrumstudy.com/)

### Otras certificaciones
Debido al éxito de Agile y [Scrum](/tags/scrum/) como marco de trabajo para gestión de producto, en los últimos años han ido apareciendo numerosas certificaciones que intentan aprovechar ese tirón. Personalmente considero que tanto EXIN Agile Scrum Master y Scrum Master Certified de Scrum Study forman parte de ese grupo (y en cierto modo también la Agile Certified Practitioner del PMI), pero no son las únicas. Entre ellas también podéis encontrar, por ejemplo, Scrum Manager o European Scrum.

## Comparativa

Dado que no es sencillo decantase a la hora de elegir una certificación Scrum Master, posiblemente sea útil ilustrar los datos mas importantes de cada una de ellas en un formato resumen. En la siguiente tabla se muestra una comparativa con datos relativos a cada una de las principales certificaciones disponibles en el mercado.

*Con el objetivo de simplificar, solamente se incluyen en la tabla comparativa las certificaciones Scrum Master más valoradas en el mercado.*

|                            | PSM I                  | CSM                    | PMI-ACP               |
| -------------------------- | :--------------------- | :--------------------- | :-------------------- |
| &nbsp;                     |                        |                        |                       |
| Año de creación            | 2009                   | 2002                   | 2011                  |
| Certificados emitidos      | 63k+                   | 378k+                  | 22k+                  |
| Popularidad                | \*\*\*\*\* (5)         | \*\*\*\*\* (5)         | \*\*\*\* (4)          |
| Material de formación      | Guía gratuita          | Material curso         | Guía gratuita         |
| Requisitos previos         | Ninguno                | Ninguno                | Acreditar exp. previa |
| &nbsp;                     |                        |                        |                       |
| Curso oficial de formación | Si                     | Si                     | No                    |
| Formación obligatoria?     | No                     | Si                     | -                     |
| Coste de la formación      | 900$-1500$             | 900$-1500$             | -                     |
| Material de formación      | Estandarizado          | No-Estandarizado       | -                     |
| &nbsp;                     |                        |                        |                       |
| Examen requerido?          | Si                     | Si (desde 2012)        | Si                    |
| Coste de examen            | 150$ (0$ con el curso) | 0$ (incl. con curso)   | 495$                  |
| Nº de preguntas examen     | 80                     | 35                     | 120                   |
| Nota mínima                | 85% (68/80)            | 69% (24/35)            | ~70%                  |
| Duración examen            | 60 minutos             | sin límite de tiempo   | 180 minutos           |
| Idioma del examen          | Inglés                 | Inglés                 | Inglés                |
| Dificultad del examen      | Intermedia             | Relativamente sencillo | Intermedia            |
| &nbsp;                     |                        |                        |                       |
| Renovación                 | No                     | Cada 2 años            | Cada 3 años           |
| Coste de renovación        | -                      | 100$                   | 30 PDUs               |
| &nbsp;                     |                        |                        |                       |
| Coste de adquisición       | 150$                   | 900$-1500$             | 495$                  |

## Conclusiones

De los datos expuestos en la comparativa podemos extraer algunas conclusiones:
- CSM y PSM I son los programas de certificación Scrum Master más reconocidos a nivel mundial, pero PMI-ACP no está lejos de ambas.
- De acuerdo a la dificultad del examen, PSM I y PMI-ACP son las certificaciones mas complejas de obtener.
- CSM tiene el coste de adquisición más alto.
- CSM tiene muchos más certificados que cualquier otro programa de certificación, pero hay que tener en cuenta que ha sido creada 8 años antes que las demás y que hasta 2012 no era obligatorio pasar un examen para obtener la certificación.
- CSM y PMI-ACP tienen un coste de renovación cada dos y tres años respectivamente. En el caso de PMI-ACP será necesario obtener 30 PDU's en formaciones y eventos aprobados por el PMI, mientras que para renovar la certificación CSM solo será necesario pagar la cuota de renovación (no es necesario examen ni curso adicional)

Espero que esta información pueda ser útil para decidir por cual certificación Scrum Master decantarse.

En caso contrario, o si quieres más información al respecto, aquí tienes algunos enlaces a otras comparativas con información interesante:
* [CSM or PSM - which Certification to choose?](http://www.agile-scrum-master-training.com/certified-scrum-master-csm-vs-professional-scrum-master-psm/)
* [Agile/Scrum Certification](https://www.benlinders.com/agile-scrum-certification/)
* [Comparativa de certificaciones ágiles – ¿Cuales son las mejores?](https://jeronimopalacios.com/2016/09/comparativa-certificaciones-agiles-cuales-las-mejores/)
* [Scrum Master Certificate. Which one should you choose?](http://webgate.ltd.uk/scrum-master-certificate/)

Más en Linkedin: https://www.linkedin.com/pulse/scrum-master-certification-comparison-which-should-i-choose-roldán/
