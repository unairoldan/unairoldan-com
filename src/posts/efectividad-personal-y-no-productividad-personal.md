---
templateKey: blog-post
title: Efectividad Personal (y no Productividad Personal)
date: 2017-08-14T21:00:00.000Z
description: >-
  Desde hace más o menos año y medio hago bastante formación con nuestros
  diferentes clientes sobre Productividad Personal, Efectividad Personal,
  Getting Things Done, etc., y sois muchos los que me habéis dicho que sería una
  buena idea escribir sobre ello.
featuredpost: false
sharethis: Lo que contribuyes no se mide por la cantidad de cosas que haces, sino por el valor aportado de lo que haces. Ya no hablamos de Productividad Personal, hablamos de Efectividad Personal
language: es
featuredimage: /img/personal-effectiveness.webp
keywords: productividad personal, efectividad personal, getting things done, estres, productividad, reuniones eficaces
tags:
  - productividad personal
  - efectividad personal
  - getting things done
  - neurociencia
  - estrés
---
Yo soy de los que opino que **aplicar la mejora continua al cómo te organizas personalmente puede cambiarte la vida** (literalmente), y dado que es algo que considero muy relevante y que utilizo en mi día a día... ¿por qué no compartirlo abiertamente?

![Efectividad personal](/img/personal-effectiveness.webp "Efectividad personal")

Llevo trabajando mi forma de organizarme a nivel personal desde siempre, pero soy relativamente nuevo siendo consciente de ello. Me explico. Desde hace muchos años intento ir mejorando la forma de organizar mi vida, y lo he ido haciendo de forma paulatina utilizando diferentes herramientas (Email, Calendario, Agendas, etc..), pero no hace mucho que descubrí que existe un campo de estudio dedicado expresamente a ello. Y esto fue hace un par de años y gracias a mi gran colega [Jordi Falguera](https://twitter.com/jordi_falguera), que en una de las habituales formaciones internas que hacemos en la [UST Agile Practice](https://twitter.com/ustagile) me descubrió el mundo de la [Productividad Personal](/tags/productividad-personal/) y Getting Things Done.

Resulta que todo eso que yo venía intentando hacer desde hace años era un campo de estudio en si mismo y **ya existían métodos definidos y probados**. ¡Que cosas! Alguien se me había adelantado.

## ¿Por qué Efectividad Personal?

Todos conocemos ya el concepto de [Trabajadores del Conocimiento](https://es.wikipedia.org/wiki/Trabajador_del_conocimiento), y entendemos que para esta ~~nueva~~ tipología de trabajo **no debemos aplicar las teorías tradicionales de gestión y de organización**; esto es, de forma muy resumida, que desde un punto de vista tradicional ser buen trabajador implica básicamente hacer muchas cosas. **Si en una hora hago 5 cosas, es mucho mejor que si en una hora hago 3 cosas. Soy más productivo**.

A nadie se le escapa que esto ya no es real. Los trabajadores del conocimiento partimos de una base en la que siempre hay más trabajo por hacer del que realmente podremos hacer nunca, por eso es mucho más importante decidir **que cosas NO vamos a hacer**, que hacerlo todo.

En otras palabras, hoy en día lo que contribuyes **no se mide por la cantidad de cosas que haces, sino por el valor aportado de lo que haces**. Ya no hablamos de Productividad, hablamos de Efectividad.

De igual forma que no tiene sentido hacerlo a nivel de trabajo, no tiene sentido enfocarnos en la Productividad a nivel personal, sino en la [Efectividad Personal](/tags/efectividad-personal/).

!#ShareThis

## Sobra actividad y faltan resultados

Llevo muchos años [trabajando en grandes organizaciones](/transformacion-digital-bbva/), y la realidad es que a día de hoy en ellas la mayor parte de las personas no son conscientes de este cambio hacía la Efectividad Personal. No sólo las personas se rigen por principios tradicionales de organización del trabajo, **lo hace la organización entera**. Y por ello seguimos encontrándonos cosas cómo que el *management* mide la productividad de las personas en base a la cantidad de cosas que hacen, o planificaciones estratégicas con ¡35 proyectos estratégicos al año!, porqué claro, tenemos que terminar todo lo que hay que hacer, y todo es importante.

Las consecuencias de esto son bastante desoladoras. No somos capaces de priorizar y de centrarnos en lo importante, lo que conlleva una **pérdida alarmante de competitividad**, y cómo además intentamos hacer muchas más cosas de las que podemos hacer, tenemos a las personas sometidas a unos **niveles de estrés cada vez más altos** y sostenidos en el tiempo.

Y esto **no es saludable**, **ni sostenible en el tiempo**.

Por ello en esta serie monográfica sobre Efectividad personal intentaré dar mi visión sobre este campo de estudio, explicando mi método personal y dando datos que para mi han sido reveladores. Y todo ello partiendo desde diferentes enfoques:

* **Efectividad personal:** Partiendo del trabajo de [David Allen](https://twitter.com/gtdguy) y su método [Getting Things Done](/tags/getting-things-done/), el cual utilizo desde hace tiempo y que es piedra angular de mi forma de organización.
* **Neurociencia aplicada:** Intentando entender cómo funciona nuestro cerebro, que es un pilar básico a la hora de poder trabajar en un plano efectivo y relajado. En este campo he tenido la suerte de dar con [Estanislao Bachrach](https://twitter.com/estanibachrach), doctorado en biología molecular y profesor de productividad y liderazgo desde un enfoque neurocientífico.
* **Gestión del estrés:** Abordando uno de los problemas más habituales en cualquier persona a día de hoy: el estrés generado por las formas de trabajo y organización ineficientes.
* **Manejo de agenda:** Siendo capaz de gestionar nuestra propia agenda, intentando gestionar nuestros compromisos y el tiempo del que disponemos de forma efectiva.

Tomen asiento, [comenzamos](/tu-cerebro-la-navaja-suiza-de-la-efectividad-personal/)...
