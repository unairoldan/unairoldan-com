---
templateKey: blog-post
title: Scrum Master certification comparison. Which should I choose?
date: 2016-09-12T10:00:00.000Z
description: >-
  Since I started working with Agile some years ago, there are tons of doubts
  that people ask me in my work with different clients, in community meetings or
  at my day to day, but there is a question that lately many people repeat me,
  and that seems like its more difficult every day for not initiated people:
  Which is the best Scrum Master certification program?
featuredpost: true
sharethis: PSM I and CSM are the most accepted worldwide and popular Scrum Master certification programs, but PMI-ACP is not far from them
language: en
featuredimage: /img/scrum-cubes.webp
keywords: scrum, agile, cerification, scrum master, scrum master certification, psm, psm i, csm, scrum.org, scrum alliance, scrum study, european scrum
tags:
  - agile
  - scrum
  - certification
  - product management
  - training
---
Given the explosion that we live in recent years with everything related to agile methodologies, and not only at the level of software development in small niche companies or start-ups, but also at [large organizations](/tags/business-agility/), it is not casual the appearance of many companies willing to be providers of Scrum Master certifications. Companies that do not necessarily have a long career in the [Agile world](/tags/agile/), and that are not backed by recognized entities, but who have created new certification programs that have flashy names, difficult to distinguish, and all of them written as three-letter acronyms, which makes it certainly difficult to differentiate between them.

![Scrum Master Certification](/img/scrum-cubes.webp "Scrum Master Certification")

Is quite easy for the people who is involved in the community to identify the most recognized certifications, but it is not a simple task for those who just arrived to the Agile world. And although there is no a direct answer for the question of which certification is better, as this depends of the preferences and possibilities of each person, it is true that exists certain information that can be analyzed to take the best decision for each personal situation.

## A brief historical introduction

As is usual, the certification programs are supported by organizations, and in the case of Scrum Master certification was the [Scrum Alliance](http://www.scrumalliance.org/), founded by [Ken Schwaber](https://twitter.com/kschwaber), [Mike Cohn](https://twitter.com/mikewcohn) and [Esther Derby](https://twitter.com/estherderby), the first organization that offered the certification program in 2002.

[Scrum Alliance](http://www.scrumalliance.org/) was the main (and the only) provider certification courses until 2009, [when Ken Schwaber leaving the organization to found Scrum.org](http://www.scrum.org/About/Origins). Few months later, Ken Schuwaber released the Professional Scrum Master certification.

Shortly thereafter, the [Project Management Intitute (PMI)](http://www.pmi.org/), which had historically disowned of agile methods makes an important change of direction including agile methodologies in version 5 of PMBoK, its framework for project management, and create in 2011 your Agile own certification. It was after this movement of the PMI when new organizations appear with new certifications Scrum Master under his arm, reaching even some of those organizations create their own methodological framework.

## Which is the best Scrum Master Certification?

To start delimiting the scope of question, these are the most globally recognized scrum master certification programs, that later we will compare in detail:

- Professional Scrum Master (PSM I) - Scrum.org
- Certified Scrum Master (CSM) - ScrumAlliance
- Agile Certified Practitioner (PMI-ACP) - PMI
- Agile Scrum Master (ASM) - EXIN
- Scrum Master Certified (SCM) - Scrum Study

All certifications are supported by different organizations, which have different certification programs related to Agile, not only for Scrum Masters, being [Scrum Alliance](http://www.scrumalliance.org/) and [Scrum.org](http://www.scrum.org/) the most widely recognized organizations in the Agile world.

!#ShareThis

### Professional Scrum Master I (PSM I)

Professional Scrum Master I is the most widely extended Scrum Master certification from Scrum.org, IMHO the most complete of all available in the market. There is the possibility to directly access the certification exam at a price of $ 150 or through a two-day training including exam fee.

It has several levels of certification, being able to access the PSM II and PSM III certification as you obtain the immediately prior accreditation.

[Professional Scrum Master I (PSM I)](https://www.scrum.org/Assessments/Professional-Scrum-Master-Assessments/PSM-I-Assessment), [Scrum.org](http://www.scrum.org/)

### Certified Scrum Master (CSM)

Certified Scrum Master is certainly the most widespread Scrum Master certification and with more trainers worldwide, and therefore has more courses available. Unlike the PSM I of Scrum.org, to obtain certification CSM is required to attend a 2-day training, thus being the highest cost of acquisition.

In addition to this, it is necessary to renew the certification every two years, paying a fee of $ 100 without any additional training or examination.

[Certified Scrum Master (CSM)](http://www.scrumalliance.org/certifications/practitioners/certified-scrummaster-csm), [Scrum Alliance](http://www.scrumalliance.org/)

### Agile Certified Practitioner (PMI-ACP)

Agile Certified Practitioner is the Scrum Master certification of Project Management Institute, and includes not only the Scrum framework but other agile methods like Kanban, Lean or eXtreme Programming. Despite being an Agile certification, the certification model and type of test is very similar to the classic PMP, very extensive and focused on theory.

Like its sister, the PMP certification, PMI-ACP certification has minimum requirements to access it: is necessary to certify at least 2000h of project experience, 1500h working on Agile projects and at least 21 hours of training in agile practices.

[Agile Certified Practitioner (PMI-ACP)](http://www.pmi.org/certifications/types/agile-acp), [PMI](http://www.pmi.org/)

### EXIN Agile Scrum Master (ASM)

We enter dark land with the Scrum Master certification of EXIN, the renowned Dutch company dedicated to providing IT certification services throughout the world. According to its website, the certification has been developed by experts from around the world, but does not name any of them. It is a certification that, beyond companies that only seek certification seal, it is not recognized in the industry.

The price of the exam is 170 € and you have to answer correctly the 65% of the questions to pass the test (26 of 40).

[Agile Scrum Master (ASM)](http://www.exin.com/NL/en/certifications/&exam=exin-agile-scrum-master), [EXIN](http://www.exin.com/)

### Scrum Master Certified (SCM)

Another certifications developed over the last time is the SCM of Scrum Study, part of VMEdu. The price of the exam is $ 450 and is available in several languages, including the possibility of taking a course with them. On their website there is no information on the difficulty of passing the exam, but they boast a 95% pass rate.

Today the SCM certification is not recognized in the industry beyond of companies that just looking to have a “certified Scrum Master.”

[Scrum Master Certified (SCM)](http://www.scrumstudy.com/scrum-master-certification-training.asp), [Scrum Study](http://www.scrumstudy.com/)

### Other certification programs
Due to the success of Agile and [Scrum](/tags/scrum/) as a framework for product management, numerous certifications have been appearing in recent years that try to take advantage of this situation. I personally consider both EXIN Agile Scrum Master and Scrum Master Certified Scrum Study to be part of that group (and to some way also Agile Certified Practitioner from PMI), but they are not the only ones. Among them you can also find, for example, Scrum Manager or European Scrum.

## Comparison

Since is not easy to decide by one certification program for the people newcomer to Agile, it was need illustrate in a simple way all the information to facilitate your decision making. And the easiest way to evaluate the different scrum master certifications is to use a comparison table with the key points between the different programs.

*To simplify, only included in the comparative table the Scrum Master certifications most valued in the market.*

|                          | PSM I                 | CSM                  | PMI-ACP                |
| ------------------------ | :-------------------- | :------------------- | :--------------------- |
| &nbsp;                   |                       |                      |                        |
| Creation year            | 2009                  | 2002                 | 2011                   |
| Certificates issued      | 63k+                  | 378k+                | 22k+                   |
| Popularity               | \*\*\*\*\* (5)        | \*\*\*\*\* (5)       | \*\*\*\* (4)           |
| Learning materials       | Free guide            | In course            | Free guide             |
| Previous requirement     | None                  | None                 | Accredit previous exp. |
| &nbsp;                   |                       |                      |                        |
| Training available       | Yes                   | Yes                  | No                     |
| Training required?       | No                    | Yes                  | \-                     |
| Training cost            | 900$-1500$            | 900$-1500$           | \-                     |
| Training materials       | Standardized          | Non-Standardized     | \-                     |
| &nbsp;                   |                       |                      |                        |
| Assessment required?     | Yes                   | Yes (since 2012)     | Yes                    |
| Assessment cost          | 150$ (0$ with course) | 0$ (incl. in course) | 495$                   |
| Assessment questions     | 80                    | 35                   | 120                    |
| Assessment passing grade | 85% (68/80)           | 69% (24/35)          | ~70%                   |
| Assessment duration      | 60 minutes            | no limit             | 180 minutes            |
| Assessment language      | English               | English              | English                |
| Assessment difficulty    | Intermediate          | Relatively easy      | Intermediate           |
| &nbsp;                   |                       |                      |                        |
| Renewal                  | No                    | Every 2 years        | Every 3 years          |
| Renewal cost             | \-                    | 100$                 | 30 PDUs                |
| &nbsp;                   |                       |                      |                        |
| Acquisition cost         | 150$                  | 900$-1500$           | 495$                   |

## Conclusions

From the above data, we can extract some evidences to consider:

* PSM I and CSM are the most accepted worldwide and popular Scrum Master certification programs, but PMI-ACP is not far from them.
* According to difficulty and passing grades, PSM I and PMI-ACP are the Scrum Master certifications most difficult to obtain.
* CSM has the highest acquisition cost.
* CSM has issued much more certificates than any other program, but should be taken into account that was created 8 years before. Also, until 2012 was not required to approve an exam to obtain the certification.
* CSM and PMI-ACP has a renewal cost every two or three years respectively. For PMP-ACP it is necessary obtain 30 PDU’s in training or events approved by PMI; In case of CSM, just have to pay to be “re-certificated” (no exam or recycling course is required).

I hope this information is useful in your decision making.

Otherwise, or if you want to have a second point of view, here you have other posts comparing Scrum Master certification on the Internet:

* [CSM or PSM - which Certification to choose?](http://www.agile-scrum-master-training.com/certified-scrum-master-csm-vs-professional-scrum-master-psm/)
* [Agile/Scrum Certification](https://www.benlinders.com/agile-scrum-certification/)
* [Comparativa de certificaciones ágiles – ¿Cuales son las mejores?](https://jeronimopalacios.com/2016/09/comparativa-certificaciones-agiles-cuales-las-mejores/)
* [Scrum Master Certificate. Which one should you choose?](http://webgate.ltd.uk/scrum-master-certificate/)

More in Linkedin: https://www.linkedin.com/pulse/scrum-master-certification-comparison-which-should-i-choose-roldán/
