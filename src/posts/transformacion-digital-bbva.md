---
templateKey: blog-post
title: Transformación digital del segundo banco español
date: 2017-02-25T11:00:00.000Z
description: >-
  Because it's 2015. Así respondía Justin Trudeau, primer ministro canadiense, a
  una periodista que le preguntaba por la importancia para él de tener un
  gabinete con equilibrio de género un número similar de mujeres y hombres.
  Aunque a algunos les pudiera parecer una pregunta difícil de argumentar, la
  respuesta era obvia. No había mucho más que explicar.
featuredpost: true
sharethis: Después de un año de transformación digital el banco reportó un aumento en sus ingresos de 10,9 por ciento, llegando a los 23,68 mil millones de €, con el programa de banca digital abanderando el reporte de resultados
language: es
featuredimage: /img/digital-banking.webp
keywords: estrategia, organización, business agility, banca, transformación digital, agile@scale, safe, less, nexus
tags:
  - business agility
  - organización
  - estrategia
  - modelo operativo
  - gestión del cambio
  - agile
  - scrum
  - recursos humanos
---
Lo mismo ocurre a día de hoy con la necesidad de cambiar de las grandes organizaciones. **¿Por qué cambiar? La respuesta es obvia, no hay mucho más que explicar.** Y quizás este sea el año en el que todas las grandes organizaciones (en realidad las que faltan, porque muchas de ellas ya han iniciado su camino con mayor o menor acierto en los últimos tiempos) **se lancen al abismo de la Transformación Digital sin muchas veces saber ni siquiera lo que significa realmente**, el impacto que tiene.

![transformación digital](/img/digital-banking.webp "transformación digital")

El año comenzaba con un artículo escrito por Mary Poppendieck titulado [The End of Enterprise IT](http://www.leanessays.com/2017/01/the-end-of-enterprise-it.html), relatando algunos aspectos del cambio a nivel organizacional en la matriz de ING, el banco holandés que entre otros países también opera en España. En él **se relata como la organización fue descubriendo de forma empírica que ese cambio les obligaría a reinventarse a nivel estructural, jerárquico y de negocio**, y que todo ello implicaría, por supuesto, el consecuente **cambio cultural** en la compañía. Y todo ello ha sido convenientemente bautizado a nivel de mercado con la etiqueta de Transformación Digital, la que quizás no sea la etiqueta más acertada, *but Business is Business*.

Mucho se ha escrito y dicho sobre la transformación de grandes organizaciones: que es imposible, que no quieren cambiar, que es fácil, yo lo hago en 20 días, que no merece la pena, etc. Curiosamente muchas opiniones de este tipo suelen ser de gente que nunca ha formado parte de un proceso de transformación *de verdad*, **a todos los niveles y no quedándose sólo en que varios equipos de IT** [hagan](https://www.slideshare.net/lazygolfer/doing-agile-isnt-the-same-as-being-agile) Scrum desde su silo en la planta 2 de uno de los edificios de la organización.

Por todo ello, en mi caso lo único que quiero es **contar una experiencia**, uniendo datos concretos y contexto, recopilando información que ya ha sido publicada e hilándolo todo desde el día a día de ser parte de esta transformación digital. Eso sí, dándole un toque especial y escribiéndolo desde el cariño que le tengo a este proyecto y a los maravillosos profesionales que han formado y forman parte de él.

## Los inicios en Banca Digital

Parecía un jueves cualquiera de Mayo, pero el café de esa mañana no iba a ser cómo el de los demás días; de hecho, hubieron muchos cafés esa mañana. Banca Digital despertaba con la noticia del año, y quizás de la década para nuestra área: **Carlos Torres, director ejecutivo de Banca Digital, había sido [elegido para ser el nuevo CEO del Banco](http://www.vozpopuli.com/economia-y-finanzas/BBVA-Carlos_Torres-Angel_Cano_0_803919649.html)**. No se haría oficial hasta ese mismo Lunes, pero a esas alturas, después de tantos meses de trabajo, nosotros ya intuíamos lo que aquello podía suponer: **La transformación digital dejaba de ser solo digital para convertirse en la nueva apuesta estratégica de la organización**. La transformación de todo [su ecosistema](http://www.elconfidencialdigital.com/dinero/CEO-BBVA-Google-Amazon-Apple_0_2484351723.html).

Todo había comenzado a finales del verano de 2014, cuando BBVA decidió [dar el salto al mundo digital](http://www.eldiario.es/edcreativo/blogs/clickbanking/metodologia-agile-scrum-bbva-fintech_6_522207788.html), y para ello inició un proyecto, por aquel entonces con unas 150 personas, **centrado en dotar de nuevas capacidades digitales y en comenzar el cambio cultural dentro de la organización**. La apuesta era clara, y a los 12 equipos que iniciaron este camino les acompañaba un equipo diferente que ayudaría a la organización a mutar para conseguir sus objetivos, el equipo de Coaches, Agentes del Cambio o*\[inserta aquí el nombre mitológico que más te guste]*. Y yo tuve (y tengo) la suerte de formar parte de [ese equipo](https://twitter.com/USTAgile/lists/coaches-team/members).

Después de un arranque muy ágil, todo transcurrió de forma rápida pero más o menos estructurada. Se comenzó a trabajar con los **equipos de producto digital para tener una cadencia de dos semanas**, sentando las bases a nivel metodológico (casi todos habían optado por [Scrum](/tags/scrum/)) y trabajando mucho la formación inicial para ir poco a poco descubriendo **con qué rol se sentía cómodo cada persona** - obviamente, trabajando desde una visión sistémica. Una vez sentadas las bases, y que el **trabajo a nivel de Portfolio de Producto** comenzó a tomar forma, no se tardó mucho en incluir a nuevos equipos, que obviamente traían bajo el brazo su producto, llegando a incluir **un total de 20 equipos seis meses después de arrancar la transformación en el área**. Entre estos equipos predominaban los productos ligados a productos bancarios, [aunque también había excepciones](http://www.expansion.com/economia-digital/companias/2015/12/22/56795576268e3ea2388b471f.html), y todos ellos estaban formados por personas especialistas en **negocio, tecnología y de otras áreas *[enablers](https://theitriskmanager.wordpress.com/2016/12/03/the-real-state-of-scaled-agile/)* en los casos en los que era necesario**.

!#ShareThis

Llegaba el momento del **diseño organizacional para garantizar la coordinación y el alineamiento**, y para ello era importante incluir equipos de áreas relevantes cómo **Riesgos**, **Seguridad** o **Legal&Compliance**, además de comenzar a trabajar con **Recursos Humanos** para temas cómo la creación de nuevos roles o los nuevos paths de carrera. Una vez estructurados los programas de negocio llegaron a banca digital los que serían los últimos equipos en incorporarse, siendo ya en total **32 equipos de producto y llegando a la cifra de [+550 personas en menos de un año](https://www.finextra.com/news/fullstory.aspx?newsitemid=28358) de transformación digital y cultural**. Todo ello dentro de un mismo Portfolio, el de Banca Digital, pero no era el único. Hacía ya unos meses que se trabajaba en la misma iniciativa en otras áreas de la organización, con otros equipos de coaches desplegados trabajando en la misma línea. Entre ellas diferentes verticales de negocio con modelos operativos muy diferentes entre sí: desde áreas de [innovación](/a-tu-cerebro-no-le-gusta-la-innovacion/) y de producto digital hasta banca de inversión, pasando por áreas *enablers* cómo equipos jurídicos o de auditoria.

Un año y tres meses después de comenzar en este nuevo reto, la iniciativa ya era una realidad en **varias áreas de la misma organización**, involucrando a muchísimas personas **que trabajaban de forma coordinada**, **entregando producto a mercado cómo máximo cada tres meses** y proveyendo a sus clientes nuevos servicios y canales digitales con los que hacer más fácil su vida diaria. ¿Eres cliente de BBVA? ¿lo has notado? No es una quimera, puede hacerse, y se ha hecho en España.

## Impacto de la transformación digital

Seamos sinceros, a nadie le importa si todos tus equipos ~~hacen~~ trabajan con [Scrum](/tags/scrum/), si tienen un Product Backlog o si el trabajo de tu equipo de Front Global es [expuesto por Google en su evento anual en Europa](https://twitter.com/cellsjs/status/734663578041602048). A nadie. Ni siquiera importa si tu equipo de [Marketing Digital es el más Agile del Banco](https://www.sistrix.es/blog/bbva-transformacion-digital-google/). No me malinterpretéis, todo esto está muy bien, y son victorias importantes a celebrar, estás en el buen camino... pero lo que de verdad va a medir el éxito de tu cambio organizacional son los resultados a nivel de negocio. **El impacto en la cuenta de resultados**.

Después de algo más de un año de transformación digital el banco reportó un **aumento en sus ingresos del 10,9%, llegando a los 23,68 mil millones de €**, con el [programa de banca digital abanderando el reporte de resultados](https://www.finextra.com/newsarticle/28421/digital-transformation-driving-earnings-at-bbva). Además de esto, **14,8 millones de clientes ya operaban con el banco a través de la banca online y mobile**, siendo éste el canal principal para 8,5 millones de clientes, prácticamente el doble que el año anterior. Pero no todo eran resultados a nivel económico; todas las métricas de transformación tuvieron una mejora muy significativa, pero especialmente las asociadas a los ciclos de *delivery* y de *customer satisfaction*, e incluso la evolución del IReNe (Índice de Recomendación Neta) permitió remontar y superar a sus principales competidores.

El banco [hacía gala de su Transformation Journey](https://accionistaseinversores.bbva.com/TLBB/micros/bbvain2015/en/), **y los resultados en la cuenta de resultados respaldaban la apuesta estratégica**. Tanto era así, que fue en ese momento cuando nos dimos cuenta que esto no había hecho más que comenzar... se abrían nuevos horizontes, lejanos, muy lejanos en algunos casos.

Así fue cómo arrancaba **la transformación global** del segundo mayor banco de España... y que también era **el sexto más grande por volumen de todo Latinoamérica**.

Más en Linkedin: [https://www.linkedin.com/pulse/digital-transformation-second-spanish-bank-unai-roldán/](https://www.linkedin.com/pulse/digital-transformation-second-spanish-bank-unai-rold%C3%A1n/)
