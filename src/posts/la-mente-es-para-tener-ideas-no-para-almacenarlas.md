---
templateKey: blog-post
title: 'La mente es para tener Ideas™, no para almacenarlas'
date: 2017-08-26T21:00:00.000Z
description: >-
  En entradas anteriores hemos hablado de la ineficacia del cerebro como sistema
  para organizarnos, y hoy vamos a hablar de las consecuencias de utilizarlo
  para ello. La principal de ellas, que utilizando el cerebro para almacenar
  ideas, no podremos usar la mente para tenerlas. Pero no es la única..
featuredpost: false
sharethis: Utilizamos el cien por cien del cerebro, pero sólo podemos utilizar el 2 por ciento al mismo tiempo
language: es
featuredimage: /img/brain-busy.webp
keywords: productividad personal, efectividad personal, cerebro, mente, memoria, cortex, neo-cortex, olvidar, olvido, repasar, recordar, organización, porcentaje del cerebro
tags:
  - productividad personal
  - efectividad personal
  - getting things done
  - neurociencia
  - estrés
---
Los científicos dicen que en los últimos 8-10 años hemos aprendido más sobre nuestro cerebro que en toda la historia de la humanidad. Gracias a los avances en Neurotecnología no invasiva, hoy en día somos capaces de observar cómo se comporta un cerebro humano por ejemplo al tocar el violín o comiendo un helado, **incluso hasta el nivel de saber que zonas y conexiones neuronales activamos en cada momento**.

![la mente](/img/brain-busy.webp "la mente")

Estas nuevas capacidades nos han permitido estudiar de forma más detenida y certera el funcionamiento del cerebro, y además de con muchos avances, también ha contribuido a [desmentir grandes mitos que existen sobre el cómo funciona cerebro](https://www.amazon.es/%C3%81gilmente-Aprend%C3%A9-funciona-potenciar-creatividad-ebook/dp/B009A81O9E) y la mente.

## ¿Qué porcentaje del cerebro utilizamos?

Esta es una pregunta que hago siempre en formaciones y talleres de [Efectividad Personal](/tags/efectividad-personal/), y es una **clara muestra de cómo los avances de la ciencia tardan en llegar al imaginario colectivo**.

Ante esta pregunta, la mayoría de las personas responden con un 10-12% de uso, [basadas en el mito que existe sobre este tema](https://es.wikipedia.org/wiki/Mito_del_10_%25_del_cerebro), mientras algunos dan porcentajes *random* basados en sus propias ideas. **La realidad es que utilizamos el 100% del cerebro**, todo *enterito*, salvo obviamente, las personas que tienen la desgracia de tener algún tipo de problema o enfermedad que les afecte esta capacidad. Utilizamos el 100% del cerebro, **pero sólo podemos utilizar el 2% al mismo tiempo**.

Otra vez, usamos el 100% del cerebro pero solamente podemos utilizar a la misma vez el 2%.

Imaginemos que nuestro cerebro son 100 bombillas led. Pues bien, nosotros somos capaces de utilizar todas esas bombillas, pero sólo somos capaces de encender al mismo tiempo 2 de ellas. Ahora cambia las bombillas por neuronas y conexiones neuronales, y así es cómo funciona el cerebro y la mente. **Podemos activar un máximo del 2% para hacer cualquier tarea que necesitemos**, y si queremos hacer varias cosas a la vez que requieren de nuestra concentración, necesitaremos apagar las neuronas dedicadas a una cosa para activar las neuronas que se ocuparán de la otra cosa que queremos hacer.

!#ShareThis

Es decir, además de tener una limitación de capacidad, **nuestro cerebro es secuencial, sólo puede hacer una cosa para la que necesitemos estar concentrados al mismo tiempo**. Y [la multitarea para este tipo de tareas es altamente ineficiente a nivel cerebral](https://www.livescience.com/59053-why-multitasking-harms-your-productivity.html)(da igual que sea un cerebro de hombre o de mujer, otro mito).

Un último dato, para no asustar a nadie. Obviamente, [tenemos muchas más de 100 neuronas y conexiones neuronales en nuestro cerebro](https://es.wikipedia.org/wiki/Neurona#Cerebro_y_neuronas). Aunque varía según la persona, se calcula que un cerebro sano tiene en torno a 86x109neuronas: es decir, unos cien mil millones de neuronas.

## Utilizando el cerebro para recordar y organizarnos

Uno de los mecanismos del cerebro más importantes a tener en cuenta de cara a nuestro propósito de Efectividad Personal es el manejo de la memoria a corto plazo, ya que cómo recordarás [está limitada a pocas cosas y poco tiempo](/tu-cerebro-la-navaja-suiza-de-la-efectividad-personal/), salvo si no lo repasas. **Y esta última parte es clave, si no lo repasas**.

Cuando almacenamos algo en la memoria a corto plazo del cerebro, y lo mantenemos más de 25-30 segundos, básicamente lo que estamos haciendo es **utilizar cierta capacidad de la mente para repasar constantemente esa información**. Es decir, cómo el cerebro a los 30 segundos lo olvidaría, cada menos segundos lo refrescamos. Y eso es pura ineficiencia.

Recordemos que nuestro cerebro funciona en modo secuencial (no puede hacer más de una cosa a la vez), con lo que refrescar esa información que hemos almacenado en nuestra memoria a corto plazo **afecta de facto a nuestra capacidad concentración**. Cada X segundos, apagamos las neuronas de lo que estamos haciendo, encendemos las que necesitamos para refrescar esa pieza de información, y volvemos a lo que estamos haciendo, ...volviendo a apagar las neuronas "del refresco de la información" y activando las que necesitamos para seguir con nuestra tarea en curso.

Y todo ello sin ni siquiera darnos cuenta. **No parece una gran idea, ¿a que no?**

Y si encima tenemos en cuenta que no solemos guardar una sola cosa en nuestra memoria, sino varias, la cosa ya se pone bastante peor. Podemos decir que **cada 20-30 segundos tenemos que interrumpir nuestra concentración varias veces** para a nivel mental ser capaces de "recordar más tiempo" las cosas que tenemos en la cabeza.

Y ahora entiendes **por qué tu cerebro se ha aficionado a olvidar cosas.. es mera supervivencia**.

![estrés](/img/information-overload-laptop.webp "estrés")

Además de esto, cómo [sólo podemos almacenar 7±2 cosas en la memoria a corto plazo](/tu-cerebro-la-navaja-suiza-de-la-efectividad-personal/), e intentamos recordar todos nuestros compromisos y tareas por hacer, **estamos claramente sobrecargando a nuestro cerebro**, lo que tiene un impacto directo en el nivel de estrés. Básicamente lo que ocurre es que nuestro cerebro activa la señal de alarma, y para poder soportar hacer más cosas de las que puede, **activa nuestros mecanismos de estrés**.

Mecanismos que sólo deberían ser utilizados en momentos muy puntuales y que de verdad son delicados.

## Liberando a la mente para tener ideas

Como ya vimos, el cerebro [no es eficaz para organizarnos porque tiende a olvidar cosas y a recordárnoslas en momento inoportunos](/tu-cerebro-la-navaja-suiza-de-la-efectividad-personal/), pero ahora además sabemos que **si utilizamos nuestro cerebro para guardar compromisos y organizarnos estamos sobrecargándolo**, y que las consecuencias de ello son una **peor productividad** a la hora de realizar cualquier tarea y que sometemos a nuestro cuerpo a un **aumento considerable del estrés**.

Ahora ya sabemos porqué, y puedes repetirlo,... **La mente es para tener Ideas™, no para almacenarlas**.

Por ello debemos buscar un sistema alternativo para organizarnos, que sea **fiable** y que además nos sea **cómodo y sencillo de utilizar**, y aquí es donde entra Getting Things Done como método.

Poneos cómodos, porque después de sentar las bases y de los necesarios preliminares (tenía que convencer a tu cerebro de que esto es de verdad buena idea, ya hablaremos de esto más adelante..), pasamos a la acción!
