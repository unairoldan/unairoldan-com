---
templateKey: blog-post
title: 'Tu cerebro, la Navaja suiza® de la Efectividad Personal'
date: 2017-08-19T21:00:00.000Z
description: >-
  Comenzamos a buscar el cambio hacia la Efectividad Personal, y la primera
  parada es la herramienta con la que todos venimos equipados de serie: tu
  cerebro.
featuredpost: false
sharethis: Tu cerebro es capaz de hacer muchas cosas, y muy bien, pero no es la mejor opción a la hora de recordar cosas y organizarte. Y esto es así por la forma en la que funciona
language: es
featuredimage: /img/brain-design.webp
keywords: productividad personal, efectividad personal, cerebro, memoria, cortex, neo-cortex, olvidar, olvido, memoria sensorial, memoria corto plazo, memoria largo plazo, navaja suiza, recordar, mecanismo de olvido, organización
tags:
  - productividad personal
  - efectividad personal
  - getting things done
  - neurociencia
  - estrés
---
![tu cerebro](/img/brain-design.webp "tu cerebro")

Y es que **la gran mayoría de las personas utilizan el cerebro para organizarse, recordar sus compromisos e incluso las tareas que tenemos que hacer**. Hasta aquí todo parece más o menos normal, es la "máquina" casi perfecta, siempre la llevamos a todos sitios y además así ejercitamos la memoria.

**Pues es una pésima idea**. Y vamos a explicar por qué..

## Un día en la oficina

Son las 7:30h de la mañana, y ya llegas tarde al trabajo. Encima el mando de la tele no funciona y has tardado un rato en encontrar el botón de detrás para apagado. Cada vez lo ponen más escondido *\-tengo que comprar pilas para el mando-*.

Mientras vas hasta la oficina, en el atasco de todas las mañanas, repasas mentalmente los temas relevantes que tienes que resolver hoy: responder al correo de María, ayudar al equipo de Marketing en su planning y terminar de preparar el informe del Portfolio. Y un tema muy importante, tienes que comentar con José los 4 puntos que salieron de la reunión de ayer, no se te puede olvidar. Pasas todo el trayecto desde casa al trabajo recordando todos los detalles de la reunión, y concienciándote*\- no me puedo olvidar-*.

Aparcas el coche, y tienes muy clara tu primera prioridad de hoy, hablar con José. Pero según entras por la puerta de la oficina...¿Qué ocurre? te olvidas completamente. Lo peor de todo, es que tomas el café con José, pero ni aun así te acuerdas de comentar con él los puntos de la reunión de ayer. Después de un largo día, de camino a casa vas a hacer unos recados, haces la compra de la semana, y cuando ya has llegado a casa y estás en la ducha, ahí aparece... tu cerebro te avisa: ¡No te olvides que tienes que hablar con José!.

*No puede ser, me ha vuelto a pasar otra vez*.

Para relajarte, que mejor que ver otro episodio en Netflix, pero la tele no enciende.. oh! también te has olvidado de comprar las pilas para el mando.

## Pero.. ¿Qué ha pasado?

Prácticamente todos hemos tenido alguna vez una [navaja suiza](https://www.google.es/search?site=imghp&tbm=isch&q=navaja+suiza). Tiene de todo, y parece la herramienta definitiva. Pero generalmente es una **solución de urgencia**. ¿Cuándo fue la última vez que cortaste un papel con las tijeras de una navaja suiza? Probablemente nunca. Lo mismo ocurre con tu cerebro, **puede hacer de todo, pero no quiere decir que sea la mejor herramienta para ciertas cosas**.

Tu cerebro es capaz de hacer muchas cosas, y muy bien, pero **no es la mejor opción a la hora de recordar cosas y organizarte**. Y esto es así por la forma en la que funciona.

En nuestro cerebro tenemos algo parecido a tres tipos de memoria: sensorial, a corto plazo y a largo plazo. Quitando la memoria sensorial, que está relacionada con lo que percibimos a través de los sentidos, la memoria a corto plazo y a largo plazo funcionan de forma muy parecida a la memoria RAM y un Disco Duro. Es decir, en la **memoria a corto plazo podemos guardar muy pocas cosas y durante un corto espacio de tiempo si no se repasa** ([hay consenso científico en que son 7±2 cosas y alrededor de 30 segundos](https://es.wikipedia.org/wiki/Memoria_(proceso)#Memoria_a_corto_plazo)), y somos capaces de acceder a ellas de manera muy rápida y eficiente. Por eso se suele decir que es la memoria operativa, **en la que tenemos las cosas a las que prestamos atención**.

En cambio, lo que almacenamos en la **memoria a largo plazo puede durarnos incluso para toda la vida, pero es mucho más lento y difícil acceder a ella**. Esas veces en nuestro día a día en las que intentamos recordar algo, pero nos cuesta, es porque estamos intentando acceder y localizar cosas en la memoria a largo plazo.

!#ShareThis

¿Qué es lo que ha pasado? Mientras ibas en el coche, camino al trabajo, estabas repasando constantemente tu "tarea" para hablar con José. Por lo tanto, toda esta información estaba almacenada en la memoria a corto plazo y era fácilmente accesible. Pero llegado el momento en el que te pones a hacer más cosas (buscar sitio, aparcar, cerrar el coche, se me olvida el portátil...), tu cerebro utilizó **sus mecanismos de olvido** para liberar espacio en la memoria a corto plazo y almacenar esa pieza de información en la memoria a largo plazo (porqué en la memoria a corto plazo tiene una capacidad muy limitada). Unos mecanismos de olvido **para los cuales a día de hoy no hay un [consenso científico](https://es.wikipedia.org/wiki/Memoria_a_corto_plazo#Duraci.C3.B3n_de_la_MCP) en cuanto su funcionamiento**.

Además de esto, **parece que tu cerebro es bastante habilidoso recordándote cosas en momento inoportunos**, rescatándolas desde tu memoria a largo plazo y haciéndote recordarlas cuando ya no puedes atenderlas. Y esto último ocurre, pero tampoco existe un consenso científico del cómo y el porqué.

## Buscando una alternativa a tu cerebro

Parece que **tu cerebro no es un sistema eficaz y fiable de organización**, de hecho, estoy seguro que ya has tenido la oportunidad de comprobarlo personalmente en numerosas ocasiones, tanto en el trabajo cómo en tu vida personal. Esas situaciones en las que nuestro cerebro olvida cosas y nos las recuerda cuando buenamente quiere.

![sistema de organización](/img/brain-open.webp "sistema de organización")

Pero esa no es la peor parte. Nuestro cerebro tiene una **capacidad limitada**, y no parece una gran idea utilizarla para recordar todo lo que tenemos que hacer, porque tener muchas cosas en la cabeza nos genera estrés. Por no mencionar que **olvidar tareas o compromisos nos generan aún más estrés**, y todo ese estrés sumado al que ya sufrimos por la forma ineficaz en la que se suele trabajar hoy en las organizaciones es un cóctel bastante nocivo. Y que puede traer consecuencias para tu salud.

Por todo ello, una parte fundamental a la hora de buscar la [Efectividad Personal](/tags/efectividad-personal/) es **buscar un sistema fiable externo que te permita organizarte**.

[No lo olvides](/la-mente-es-para-tener-ideas-no-para-almacenarlas/): **Tu cerebro es para tener ideas, no para almacenarlas**.
