---
templateKey: blog-post
title: A tu cerebro no le gusta la Innovación
date: 2017-09-16T21:00:00.000Z
description: >-
  Hace ya mucho tiempo que la innovación está de moda. Da igual de qué sector o
  ámbito de la vida se trate, ahora mismo innovar es un valor añadido. Es algo
  que toda empresa quiere conseguir o que toda persona desea para sí mismo.
  Pero, ¿qué es innovar? ¿cómo se hace?
featuredpost: false
sharethis: Tu cerebro te engaña, y lo hace queriendo. Pero lo hace por tu bien. No es que no le guste la innovación, es que no quiere que te coma el leopardo
language: es
featuredimage: /img/brain-bulb.webp
keywords: productividad personal, efectividad personal, cerebro, memoria, innovación, evolución, gestión del cambio, innovar
tags:
  - productividad personal
  - efectividad personal
  - neurociencia
  - gestión del cambio
  - personas
---
Existen muchas teorías acerca de la innovación, pero a día de hoy ya existe un consenso mas o menos claro en que Innovar es **crear algo nuevo e introducirlo en un [mercado](https://es.wikipedia.org/wiki/Mercado) con éxito** - y no tiene porqué ser con ánimo de lucro-.

No sólo tiene que ser novedoso, sino que además alguien debe querer tenerlo y/o utilizarlo.

![cerebro](/img/brain-bulb.webp "cerebro")

Y además de existir un acuerdo sobre que es la innovación, también está claro cuál es el principal ingrediente para conseguir esa innovación: **las personas**. **Personas con conocimiento, experiencia e iniciativa** que sean capaces de crear [nuevas ideas](/la-mente-es-para-tener-ideas-no-para-almacenarlas/) que mejoren en algo la vida de los demás. Pero esto último no es sencillo.

Y vamos a explicar por qué...

## Tu cerebro es un órgano prehistórico

La naturaleza es sabia, y por ello **basa la evolución de las especies en las necesidades primarias de éstas**. Un ejemplo de ello puede ser cómo poco a poco nuestra dentadura se ha ido haciendo más pequeña y con menos fuerza para morder, todo ello a consecuencia del descubrimiento y la [capacidad del ser humano para dominar el fuego](https://es.wikipedia.org/wiki/Dominio_del_fuego_por_los_primeros_humanos), entre otras cosas para cocinar los alimentos.

Del mismo modo que cambió nuestra dentadura por el descubrimiento del fuego, nuestro cerebro también ha ido cambiando. Además de un considerable **aumento de tamaño del cerebro** y de la cavidad craneal, **el cerebro ha ido mejorando, automatizando y haciendo más eficientes las tareas y necesidades más importantes para nuestra especie**.

Un ejemplo de ello puede ser la mejora en la habilidad en nuestras manos o el don de la comunicación. La capacidad de hablar entre nosotros nos ha convertido en el animal más social que existe, y ha sido de los últimos avances significativos de nuestra especie respecto a otras. Se calcula que esto ocurrió hace aproximadamente 1,6 millones de años, pero **todavía a día de hoy nuestro cerebro está cambiando para ser capaz de ser más eficiente para ello**. De hecho, hoy en día [la memoria visual, auditiva y la comprensión del habla aún comparten la misma zona cerebro](/tu-cerebro-la-navaja-suiza-de-la-efectividad-personal/), pese a ser una de las características más utilizadas y diferenciadoras del ser humano respecto a otras especies.

Tu cerebro es un órgano prehistórico que va evolucionando poco a poco, y lo ha hecho siempre en base a las **necesidades de cada momento** histórico. **Y no sólo la innovación no ha sido una necesidad, sino que a lo largo de la historia ha sido duramente castigada**.

!#ShareThis

## A tu cerebro no le gusta la Innovación

Durante 60 millones de años a los humanos nos han querido comer. Primero fueron los dinosaurios, luego los cocodrilos y después los leopardos, pero siempre hemos vivido en un entorno hostil donde nosotros éramos el alimento. **Y en un entorno así fallar o probar algo nuevo que no funcionara tenía un castigo severo: ser cazado**.

A la persona que inventó la lanza, y que posibilitó que la especie evolucionara, en su siguiente intento de innovar fracasó y se lo comieron. Los innovadores y los que arriesgaban fueron comidos por el leopardo, **y nosotros somos descendientes de los que fueron cuidadosos**.

Y los cuidadosos **no innovaban**, **eran predecibles** e **intentaban hacer las cosas sin correr el más mínimo riesgo**.

![innovación y riesgo](/img/risk-jump.webp "innovación y riesgo")

Que la evolución del cerebro cómo órgano se base en necesidades primarias parece una gran idea. La pena es que esas necesidades primarias han sido mucho más tiempo **intentar sobrevivir** a un entorno hostil, en el que el peligro era ser comido (hablamos de millones de años), **que ser capaces de innovar o resolver sudokus**.

Nuestro cerebro ha estado evolucionando desde tiempos inmemoriales evitando la innovación, y aunque ahora el mundo es diferente, **300 o 500 años de "modernidad" es una minucia al lado de millones de años de evolución**.

Tu cerebro te engaña, y lo hace queriendo. Pero lo hace por tu bien.\
**No es que no le guste la innovación, es que no quiere que te coma el leopardo**.

## La Innovación y el Cambio Organizacional

Alguno puede pensar qué ha pasado mucho tiempo desde la pre-historia, pero la realidad es que a día de hoy [muchas empresas de nuestro entorno aún viven en esa pre-historia](/tags/efectividad-personal/). El fallo se castiga duramente, y la cultura que se ha creado alrededor de **esa baja tolerancia al fallo implica directamente una falta crónica de innovación**.

Si ya nuestro cerebro tiene las defensas activadas y es cuidadoso de que no innovemos, si además en las organizaciones se señala a las personas cada vez que fallan, **tenemos el mejor cóctel para acabar con cualquier intento de hacer las cosas de forma diferente**.

Estamos creando el **caldo de cultivo perfecto para matar la innovación**. Nuestras empresas se comportan como si viviéramos en el Cretácico, pero la gran diferencia es que a día de hoy ya no hay dinosaurios...

¿O a lo mejor sí?
