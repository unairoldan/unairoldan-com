---
templateKey: index-template
title: Estrategia, Organización y Business Agility
description: 'Business Agility desde las trincheras: Estrategia, Organización, Transformación digital, Personal Effectiveness, Agile, Scrum, Kanban y otros'
keywords: estrategia, organización, trabajo remoto, teletrabajo, diseño organizacional, transformación digital, business agility, productividad personal, personal effectiveness, scrum, agile, kanban, safe, less, nexus, unai, roldan, unai roldan
image: /img/blog-index-es.webp
language: es
---
