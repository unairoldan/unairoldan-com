import React from 'react'
import { kebabCase } from 'lodash'
import { Helmet } from 'react-helmet'
import { Link, graphql } from 'gatsby'

import Header from '../../components/Header'
import Layout from '../../components/Layout'

const TagsPage = ({
  data: {
    allMarkdownRemark: { group },
    site: {
      siteMetadata: { title, description, siteUrl, keywords },
    },
  },
}) => (
  <Layout>
    <div>
      <Helmet title={`Tags | ${title}`}>
            
        <meta name="description" content={`${description}`} />
        <meta name="keywords" content={`${keywords}`} />
        <link rel="canonical" href={`${siteUrl + '/tags/'}`}  />

        <meta name="twitter:title" content={`Tags | ${title}`} />
        <meta name="twitter:description" content={`${description}`} />
        <meta name="twitter:image" content={`${siteUrl + '/img/blog-index-es.webp'}`} />

        <meta property="og:type" content="blog" />
        <meta property="og:title" content={`Tags | ${title}`} />
        <meta property="og:url" content={`${siteUrl + '/tags/'}`}  />
        <meta property="og:description" content={`${description}`} />
        <meta property="og:image" content={`${siteUrl + '/img/blog-index-es.webp'}`} />

      </Helmet>

      <Header 
        title={title}
        image= {siteUrl + '/img/blog-index-es.webp'}
      />

      <section className="section">
        <div className="container">
          <div className="content">
            <h1 className="title is-size-2 is-bold-light">Tags</h1>
            <ul className="taglist"
              itemScope itemType="https://schema.org/BreadcrumbList">
              {group.map((tag, index) => (
                <li key={tag.fieldValue} itemProp="itemListElement" itemScope itemType="https://schema.org/ListItem">
                  <Link to={`/tags/${kebabCase(tag.fieldValue)}/`} itemProp="item">
                    <span itemProp="name">{tag.fieldValue}</span> ({tag.totalCount})
                  </Link>
                  <meta itemProp="position" content={index} />
                </li>
              ))}
            </ul>
          </div>
        </div>
      </section>
    </div>
  </Layout>
)

export default TagsPage

export const tagPageQuery = graphql`
  query TagsQuery {
    site {
      siteMetadata {
        title
        description
        siteUrl
        keywords
      }
    }
    allMarkdownRemark(limit: 1000) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`
