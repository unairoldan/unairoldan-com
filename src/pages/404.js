import React from 'react'
import Layout from '../components/Layout'

const NotFoundPage = () => (
  <Layout language="es">
    <div
      className="full-width-image margin-top-0"
      style={{
        backgroundImage: `url('/img/blog-index-es.webp')`,
        backgroundPosition: `top left`,
        backgroundAttachment: `fixed`,
        height: '300px',
      }}
    >
      <div
        style={{
          display: 'flex',
          height: '150px',
          lineHeight: '1',
          justifyContent: 'space-around',
          alignItems: 'left',
          flexDirection: 'column',
        }}
      >
        <h1
          className="has-text-weight-bold is-size-3-mobile is-size-2-tablet is-size-1-widescreen"
          style={{
            boxShadow:
              'rgb(214,64,0,.6) 0.5rem 0px 0px, rgb(214,64,0,.6) -0.5rem 0px 0px',
            backgroundColor: 'rgb(214,64,0,.6)',
            color: 'white',
            lineHeight: '1',
            padding: '0.25em',
          }}
        >
          404 - Página no encontrada
        </h1>
        <p
          className="has-text-weight-bold"
          style={{
            boxShadow:
              'rgb(214,64,0,.6) 0.5rem 0px 0px, rgb(214,64,0,.6) -0.5rem 0px 0px',
            backgroundColor: 'rgb(214,64,0,.6)',
            color: 'white',
            lineHeight: '1',
            padding: '0.25em',
            textAlign: 'center',
          }}
        >
          404 - Page not found
        </p>
        <meta http-equiv = "refresh" content = "2; url = https://www.unairoldan.com" />
      </div>
    </div>
  </Layout>
)

export default NotFoundPage
