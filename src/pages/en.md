---
templateKey: index-template
title: Strategy, Organization and Business Agility
description: 'Business Agility from the Trenches: Strategy, Organization, Digital Transformation, Personal Effectiveness, Agile, Scrum, Kanban & others'
keywords: strategy, organization, remote work, remote teams, organizational design, digital transformation, business agility, personal productivity, personal effectiveness, scrum, agile, kanban, safe, less, nexus, unai, roldan, unai roldan
image: /img/blog-index-en.webp
language: en
---
