# UnaiRoldan.com

Personal website

## Technology

**Technology / Frameworks**
- [JAMstack architecture](https://jamstack.org)
- [NodeJS](https://nodejs.org/)
- [React](https://reactjs.org/)
- [Gatsby JS](https://www.gatsbyjs.org/)
- [Netlify](https://www.netlify.com/) 
- [Jest](https://jestjs.io/)

**SEO / Addons**
- [Disqus](https://disqus.com/)
- [Schema.org](https://schema.org/)

**Quality & CI/CD**
- [GitLab](https://gitlab.com/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [SonarQube](https://www.sonarqube.org/)
